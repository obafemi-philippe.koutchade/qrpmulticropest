Package: qrpmulticropest
Title: Quadratic random parameters multicrop
Version: 0.0.0.9000
Authors@R: c(person("Obafemi Philippe", "Koutchade", role = c("aut", "cre","cph"),
                     comment = c(ORCID = "0000-0001-7327-3139"),
                     email = "obafemi-philippe.koutchade@inrae.fr"),
              person("Fabienne", "Femenia", role = "aut",
                      email = "fabienne.femenia@inrae.fr"),
              person("Alain", "Carpentier", role = "aut",
                     email = "alain.carpentier@inrae.fr"))
Description: Framework for estimating random parameters multicrop models: quadratic specification described in 
  Koutchade et al., (2024) and nested specification described in Koutchade et al., (2021). Joint estimation of yield sypply equations, 
  demand inputs uses equations and acreage choice model based on SAEM algorithm.
License: GPL (>= 3)
Encoding: UTF-8
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.3.1
Biarch: true
Depends: 
    R (>= 3.4.0)
Imports:
    LearnBayes,
    MASS,
    Matrix,
    dplyr,
    matrixStats,
    matrixcalc,
    future,
    future.apply,
    methods,
    Rcpp (>= 0.12.0),
    RcppParallel (>= 5.0.1),
    rstan (>= 2.18.1),
    rstantools (>= 2.4.0),
    ks,
    plm
LinkingTo: 
    BH (>= 1.66.0),
    Rcpp (>= 0.12.0),
    RcppEigen (>= 0.3.3.3.0),
    RcppParallel (>= 5.0.1),
    rstan (>= 2.18.1),
    StanHeaders (>= 2.18.0)
SystemRequirements: GNU make
LazyData: true
