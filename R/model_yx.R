
#' @title Fitting Random Parameters Multicrop Model
#'
#' @description  `rp_model_yx` is used to fit a random parameter multicrop model  proposed in Koutchade et al. (2021) with more than one input.
#'
#' @param data name of the data frame or matrix containing all the variables included in the model
#' @param idtime  first (individual) and second (time) dimensions of the panel data.
#' @param weight weight
#' @param crop_yield vector of variables containing crop yields (t/ha)
#' @param crop_input list of vectors of variables containing input uses per crop (unit/ha)
#' @param crop_price vector of variables containing crop prices(euro/t)
#' @param crop_input_price  list of vectors of variables containing input prices per crop (euro/unit)
#' @param crop_acreage  vector of variables containing crop acreage (ha)
#' @param crop_subsidy vector of variables containing crop subsidies per ha
#' @param crop_yield_input_indvar vector of variables used to control for observed (individual and/or temporal) characteristics in yield equations
#' @param crop_rp_indvar  vector of variables used to control for observed (individual) characteristics in random parameters model
#' @param distrib_method_beta_yx assumption on the distribution of random parameters 'beta_y': "lognorm (log-normal distribution) and "norm" (normal distribution).Default="lognorm"
#' @param distrib_method_alpha_x assumption on the distribution of random parameters 'alpha_x': "logchol (log cholesky transformation) and "spherical" (spherical parameterization of cholesky decomposition of price effect parameters). Default="logchol"
#' @param sim_method method used to draw the random parameters in the simulation step of the estimation process: "MH" (Metropolis Hasting with) , "MHI" (independant Metropolis Hasting), "MHRW" (Metropolis Hasting Random Walk) or "IS" (Importance Sampling)
#' @param calib_method method used to calibrate the random parameters for each individual once the model converged: "CMODE" (conditional mode), "CMEAN" (conditional mean) or "sim-saem" (last simulation of the saem algorithm)
#' @param saem_control list of options for the SAEM algorithm. See 'Details
#' @param par_init initial parameters, a list
#' @param crop_name vector of crops' names
#'
#' @importFrom stats lm
#' @details
#' An SAEM algorithm is used to perform the estimation of input uses per crop. Different options can be specified by the user for this algorithm in the saem_control argument
#' The saem_control argument is a list that can supply any of the following component
#'
#' * `nb_iter`:  Maximum total number of iterations. Default=500
#' * `nb_burn_saem`:  Number of iterations of the burn-in phase where individual parameters are sampled
#'    from their conditional distribution using sim_method and the initial values for model parameters without update these parameters. Default=10
#' * `nb_SA`:    Number of iteration in the First stage of estimation where algorithm explore parameters space without memory. The parameter that controls the convergence of the algorithm is set to 1. Default=200.
#' * `nb_Temp`:  Number of iteration of iterations where tempering approach is used. Algorithm tries to escape local maxima if doTempering=TRUE. Default=300. Note that nb.burn+nb.SA must be inferior to K.RS and nb_Temp must inferior to nb.iter
#' * `toler`: Tolerance value for the convergence. Default 1.e-5
#' * `estim_rdraw`: number of random draws in the estimation process when sim_method="MHI". Default=20.
#' * `calib_rdraw`: number of random draws in the calibration process. Default=500
#' * `stde_rdraw`: number of random draws for computation of estimation standard errors. Default=500
#' * `p_SA`: parameter determining determing step sizes in the Stochastic Approxiation (SA) step. Must be comprise between 0 and 1. Default=0.8
#' * `upper_sph`: allow to define the interval of parameters of spherical transformation. Default= pi
#' * `doTempering`: logical. If TRUE the tempering approach proposed by (Allassonnière and Chevallier, 2021) is used to avoid convergence to local maxima. Default=TRUE
#' * `showProgress`: logical. If TRUEthe evolution of the estimation process is displayed graphically at the bottom of the screen. Default=TRUE
#' * `showIterConvLL`: logical. If TRUE iteration number and convergence value are displayed during the estimation process. Default=FALSE
#' @md
#'
#' @return `rp_model_yx` returns a list with the following components:
#' \itemize{
#' \item{`est_pop`: list of estimation results: parameters, standard error, conv.indicators, ...  }
#' \item{`beta_b_calib_list`: list of predicted random parameters }
#' \item{`est_calib`: list of calibrated random parameters, predicted yield and predicted input uses }
#' \item{`call`: copy of the function call }
#' \item{`opt`: list of saem algorithm options used }
#' \item{`data_list`:copy of list of individual data used }
#' }
#'
#' @references Koutchade, O. P., Carpentier A. and Femenia F. ()
#'
#' @export
rp_model_yx <- function(data,
                          idtime,
                          weight = NULL,
                          crop_yield,
                          crop_input,
                          crop_acreage,
                          crop_price = NULL,
                          crop_input_price = NULL,
                          crop_subsidy = NULL,
                          crop_yield_input_indvar = NULL,
                          crop_rp_indvar = NULL,
                          distrib_method_beta_yx = c("lognormal","normal","censored-normal"),
                          distrib_method_alpha_x = c("spherical","logchol"),
                          sim_method = c("mhrw_imh","mhrw", "marg_imh", "map_imh","nuts"),
                          calib_method = c("cmode","cmean"),
                          saem_control = list(),
                          par_init = list(),
                          crop_name = NULL){

  crop_acreage_sh <- paste0(crop_acreage, sep="_", "sh")

  namesInmodel <- list(crop_yield = crop_yield,
                       crop_acreage = crop_acreage,
                       crop_input = crop_input,
                       crop_price = crop_price,
                       crop_input_price = crop_input_price,
                       crop_subsidy = crop_subsidy,
                       crop_yield_input_indvar = crop_yield_input_indvar,
                       crop_rp_indvar = crop_rp_indvar,
                       distrib_method_beta_yx = distrib_method_beta_yx,
                       distrib_method_alpha_x = distrib_method_alpha_x,
                       calib_method = calib_method)

  # check if crop_input and crop_input_price are a list

  # check if length of each coponent of crop_input and crop_input_price has the same length with crop yield

  # check if 'data' argument has been specified

  if (missing(data))
    data <- NULL

  no.data <- is.null(data)

  if (no.data) {
    data <- sys.frame(sys.parent())
  } else {
    if (!is.data.frame(data))
      data <- data.frame(data)
  }

  ID_1 <- ID_2 <- NULL
  data[,c("ID_1","ID_2")] <- data[,idtime]
  data$ID_1 <- as.factor(data$ID_1)
  data$ID_2 <- as.factor(data$ID_2)

  # create crop_acreage_sh
  data$total_acreage <- rowSums(as.matrix(data[, crop_acreage]))

  if(any(crop_acreage_sh %in% names(data))){
    data = data[,!(names(data) %in% crop_acreage_sh)]
    data[, crop_acreage_sh] <- data[, crop_acreage] / rowSums(as.matrix(data[, crop_acreage]))
  }else{
    data[, crop_acreage_sh] <- data[, crop_acreage] / rowSums(as.matrix(data[, crop_acreage]))
  }

  noDms <- names(data) # get variable name in data

  # check if reference crop is always produced
  if(any(data[,crop_acreage[1]] == 0))
    stop("Reference  crop must always be produced :", paste(crop_acreage[1],">0"))

  # check that 'idtime', 'crop_acreage' arguments have been specified

  if(is.null(idtime))
    stop(" argument 'idtime' must be specified")
  if(length(noNms <- idtime[!idtime %in% noDms]))
    stop("unknown names in argument 'idtime': ", paste(noNms, collapse = ", "))

  # match distrib and  sim_method

  distrib_method_beta_yx <- match.arg(distrib_method_beta_yx)
  distrib_method_alpha_x <- match.arg(distrib_method_alpha_x)
  sim_method             <- match.arg(sim_method)
  calib_method           <- match.arg(calib_method)

  # other algorithm arguments definition

  opt    <- list(nb_SA = 200,
                 nb_smooth = 200,
                 nb_burn_saem = 10,
                 nb_burn_sim = 30,
                 estim_chains = 1,
                 toler = 1.e-5,
                 estim_rdraw= 100,
                 stde_rdraw = 1000,
                 calib_rdraw = 100,
                 warmup = 1000,
                 calib_chains = 1,
                 p_SA = 0.75,
                 # spherical.method=FALSE,
                 lo_spher_x = 0,
                 up_spher_x = pi,
                 sigma_constr = 0,
                 showIterConvLL = TRUE,
                 doTempering = TRUE,
                 doTemp_param = c(0.5,-4,2,4),
                 showProgress = TRUE,
                 cov_rp_method = "FULL",
                 score_in_est = 0)


  ## check for saem_control argument and update opt

  nmsO <- names(opt)
  opt[(namo <- names(saem_control))] <- saem_control
  if (length(noNms <- namo[!namo %in% nmsO]))
    warning("unknown names in 'saem_control' arguments: ", paste(noNms, collapse = ", "))

  opt$nb_iter <-  opt$nb_burn_saem + opt$nb_SA + opt$nb_smooth
  opt$nb_temp <- opt$nb_burn_saem + round(opt$nb_SA /2)

  ## check for resamp.method argument

  # resamp.method <- opt$resamp_method
  # if(!is.null(resamp_method) && length(noNms <- resamp_method[!resamp.method %in% c("MNL","SY","SR")]))
  #   stop(" unknown names of argument 'resamp.method': ", paste(noNms, collapse = ", "))

  cov_rp_method <- opt$cov_rp_method
  if(!is.null(cov_rp_method) && length(noNms <- cov_rp_method[!cov_rp_method %in% c("DIAG","FULL","FACTOR")]))
    stop(" unknown names of argument 'cov_rp_method': ", paste(noNms, collapse = ", "))

  ## check for relation between nb.iter, nb.SA, nb.Only.YX and nb.YXS
  ## add ?

  ## update opt accounting for 'saem_control' argument
  opt$distrib_method_beta_yx  <- distrib_method_beta_yx
  opt$distrib_method_alpha_x  <- distrib_method_alpha_x
  opt$sim_method              <- sim_method
  opt$calib_method            <- calib_method

  ### and convert distrib_method variables in STAN distrib_method variable

  if(distrib_method_beta_yx == "lognormal"){
    opt$distrib_method_beta_yx_stan <- 1
  }else if(distrib_method_beta_yx == "normal"){
    opt$distrib_method_beta_yx_stan <- 2
  }else if(distrib_method_beta_yx == "censored-normal"){
    opt$distrib_method_beta_yx_stan <- 3
  }

  if(distrib_method_alpha_x == "spherical"){
    opt$distrib_method_alpha_x_stan <- 1
  }else if(distrib_method_alpha_x == "logchol"){
    opt$distrib_method_alpha_x_stan <- 2
  }


  ####################################

  ## arrange data given idtime,
  #data   <- dplyr::arrange_at(data, idtime)
  data   <- dplyr::arrange(data, ID_1, ID_2)

  ## check if each individual is present at least 3 times in data
  data <- dplyr::mutate(dplyr::group_by(data, dplyr::across(dplyr::all_of(idtime[1]))), rowCount = dplyr::n())

  if(any(data$rowCount < 3)){
    # data    <- subset(data, data$rowCount >= 3 )
    # warning("\n only individual presents at least 3 times are considered \n")
    stop("\n Individuals may be present at least 3 times \n")
  }

  ## check that  arguments have no missings
  vardataname <- c(idtime,
                   crop_yield,
                   crop_acreage,
                   unlist(crop_input),
                   crop_price,
                   unlist(crop_input_price),
                   unique(unlist(crop_rp_indvar)),
                   unique(unlist(crop_yield_input_indvar)))

  if (any(is.na(data[,vardataname])))
    stop("arguments should not contain any NAs.")

  # check if crop price argument has no zero value
  if (any(data[,crop_price] == 0))
    stop("arguments crop_price should not contain any 0.")

  #############################################################################
  nb_crop     <- length(crop_yield)
  nb_input    <- length(crop_input)
  nb_eq_yx    <- nb_crop + nb_input * nb_crop # number of equations for yields and inputs
  hh          <- levels(factor(as.matrix(data[,idtime[1]])))
  nb_id       <- length(hh) # number of individual (farmers)

  if(is.null(crop_name) ){
    crop        <- paste0("crop", sep = "_", 1:nb_crop) # crop name
  }else if(length(crop_name) == nb_crop ){
    crop        <- crop_name
  }else{
    stop("Length of crop_name must be ", nb_crop)
  }

  nb_rp_yx    <- nb_crop*(nb_input + 1) + nb_crop * nb_input * (nb_input + 1) / 2
  nb_rp       <- nb_rp_yx

  # define name of random parameters
  ##################################
  matinput_name <- t(sapply(1:nb_input, function(k) paste0("alpha_x",k, sep="",1:nb_input)))
  matinput_name <- ks::vech(matinput_name)

  matacr_name <- t(sapply(2:(nb_crop), function(k) paste0("alpha_Bs_",k, sep="",2:(nb_crop))))
  matacr_name <- ks::vech(matacr_name)

  beta_b_y_name  <- paste0("beta_y", sep="_",crop)
  beta_b_x_name  <- lapply(1:nb_input, function(k) paste0(paste("beta_x",k,sep=""), sep="_",crop))
  beta_b_ax_name <- unlist(lapply(1:nb_crop, function(k) paste0(matinput_name, sep="_",crop[k])))


  beta_b_name    <- c(beta_b_y_name,unlist(beta_b_x_name),beta_b_ax_name)
  beta_b_name    <- list(beta_b_y_name = beta_b_y_name,
                         beta_b_x_name = beta_b_x_name,
                         beta_b_ax_name = beta_b_ax_name )


  if(is.list(crop_yield_input_indvar)){
    beta_d_y_name  <- unlist(lapply(1:nb_crop, function(k) paste0("delta_y_", crop[k], sep="_", crop_yield_input_indvar[[k]])))
    beta_d_x_name  <- lapply(1:nb_input, function(k) unlist(lapply(1:nb_crop, function(i) paste0("delta_x", k,"_", crop[i], sep="_", crop_yield_input_indvar[[i]]))))
  }else{
    beta_d_y_name  <- unlist(lapply(1:nb_crop, function(k) paste0("delta_y_", crop[k], sep="_", crop_yield_input_indvar)))
    beta_d_x_name  <- lapply(1:nb_input, function(k) unlist(lapply(1:nb_crop, function(i) paste0("delta_x", k,"_", crop[i], sep="_", crop_yield_input_indvar))))
  }

  # if(is.list(crop_price_indvar)){
  #   beta.d.p_name  <- unlist(lapply(1:nb_crop, function(k) paste0("delta_s_crop",k, sep="_", crop_price_indvar[[k]])))
  # }else{
  #   beta.d.p_name  <- unlist(lapply(1:nb_crop, function(k) paste0("delta_s_crop",k, sep="_", crop_price_indvar)))
  # }

  beta_d_yx_name    <- list(beta_d_x_name = beta_d_x_name, beta_d_y_name = beta_d_y_name)


  zdeltay_name <- paste0("zdeltay", sep="_",crop)
  zdeltax_name <- lapply(1:nb_input, function(k) paste0("zdeltax", k, sep="_",crop))
  zdeltaxy_name <- c(unlist(zdeltax_name), zdeltay_name)
  zdeltas_name <- paste0("zdeltas", sep="_",crop[-1])
  zdeltayx_name <- list(zdeltay_name = zdeltay_name, zdeltax_name = zdeltax_name)

  opt$beta_b_name     <- beta_b_name
  #opt$beta.b.d_name   <- beta.b.d_name
  opt$beta_d_yx_name     <- beta_d_yx_name
  opt$zdeltayx_name  <- zdeltayx_name
  opt$crop_yield_name <- crop_yield
  opt$crop_input_name <- crop_input

  opt$nb_rp_yx        <- nb_rp_yx
  opt$nb_rp           <- nb_rp
  opt$nb_eq_yx        <- nb_eq_yx
  opt$nb_input        <- nb_input
  opt$nb_crop         <- nb_crop
  opt$crop            <- crop

  ##############################################################################

  # regime in the data
  mat_Reg               <- data[,crop_acreage]
  mat_Reg[(mat_Reg)>0]  <- 1
  mat_Reg[(mat_Reg)<=0] <- 0

  Regime <- Freq <- regime_num <- NULL
  reg_name          <- paste0("R",sep="", 1:nb_crop)
  colnames(mat_Reg) <- reg_name
  #mat_Reg           <- mutate(mat_Reg, Regime = paste(R1, R2, R3, R4, R5, R6, R7))
  mat_Reg$Regime    <- matrix(do.call(paste0, as.data.frame(mat_Reg[,reg_name])))
  #
  RegCount <- table(Regime = mat_Reg$Regime)
  RegCount <- as.data.frame(RegCount)#Freq as variable and Regime
  RegCount <- dplyr::arrange(RegCount,-Freq)## sort by decreasing
  RegCount <- dplyr::arrange(RegCount, dplyr::desc(Regime) )
  RegCount <- dplyr::mutate(RegCount, regime_num=dplyr::row_number())
  mat_Reg  <- dplyr::arrange(mat_Reg, Regime)
  mat_Reg  <- merge(mat_Reg,RegCount, by = "Regime")
  ### Construction des relation regime num
  doublons <- which(duplicated(mat_Reg[,"regime_num"]))
  mat_Reg  <- mat_Reg[-doublons,c("regime_num",reg_name)]
  mat_Reg  <- dplyr::arrange(mat_Reg, regime_num )
  mat_Reg  <- as.matrix(mat_Reg[,c("regime_num",reg_name)])
  mat_regime <- t(sapply(1:nrow(mat_Reg), function(t) mat_Reg[t,reg_name]))
  opt$mat_regime <- mat_regime
  ##############################################################################

  ##############################################################################
  ## identification of the position of each control variable
  ##############################################################################
  if(is.list(crop_yield_input_indvar)){
    un_z_crop_yx <- unique(unlist(crop_yield_input_indvar))
    ##define the zpos_crop_yx : give the position of variable of each crop in un_z_crop_yx
    zpos_crop_yx <- lapply(1:nb_crop, function(k) match(crop_yield_input_indvar[[k]],un_z_crop_yx))
  }else{
    un_z_crop_yx <- unique(unlist(crop_yield_input_indvar))
    zpos_crop_yx <- lapply(1:nb_crop, function(k) match(crop_yield_input_indvar,un_z_crop_yx))
  }
  opt$zpos_crop_yx <- zpos_crop_yx

  ##
  # if(is.list(crop_price_indvar)){
  #   un_z_crop_p <- unique(unlist(crop_price_indvar))
  #   zpos_crop_p <- lapply(1:(nb_crop), function(k) match(crop_price_indvar[[k]],un_z_crop_p))
  # }else{
  #   un_z_crop_p <- unique(unlist(crop_price_indvar))
  #   zpos_crop_p <- lapply(1:nb_crop, function(k) match(crop_price_indvar,un_z_crop_p))
  # }
  # opt$zpos_crop_p <- zpos_crop_p


  #data    <- dplyr::arrange(data, !!!idtime)
  data    <- dplyr::arrange(data,ID_1, ID_2)
  data_balanced <- data[,idtime]
  data_balanced <- dplyr::mutate(data_balanced, IND = 1)
  data_balanced <- plm::make.pbalanced(data_balanced)
  data_balanced[is.na(data_balanced[,"IND"]),"IND"] <- 0
  data  <- dplyr::arrange(data,ID_1, ID_2)
  # data   <- dplyr::arrange_at(data, idtime)
  data_list <- future.apply::future_lapply(1:nb_id, function(i){
    idtim   <- as.matrix(data[data[,idtime[1]]==hh[i], idtime])
    yit     <- as.matrix(data[data[,idtime[1]]==hh[i], crop_yield])
    xit     <- lapply(1:nb_input, function(k) as.matrix(data[data[,idtime[1]]==hh[i], crop_input[[k]]]))
    wit     <- lapply(1:nb_input, function(k) as.matrix(data[data[,idtime[1]]==hh[i], crop_input_price[[k]]]))
    pit     <- as.matrix(data[data[,idtime[1]]==hh[i], crop_price])
    spit    <- as.matrix(data[data[,idtime[1]]==hh[i], crop_acreage])
    shit    <- as.matrix(data[data[,idtime[1]]==hh[i], crop_acreage_sh])
    subit   <- as.matrix(data[data[,idtime[1]]==hh[i], crop_subsidy])
    z_yxit  <- as.matrix(data[data[,idtime[1]]==hh[i], unique(unlist(crop_yield_input_indvar))])

    z_yxit_list    <- lapply(1:nb_crop, function(k) as.matrix(data[data[,idtime[1]]==hh[i], crop_yield_input_indvar[[k]]]))

    mat_Zyx <- Reduce("rbind",lapply(1:nrow(yit), function(t){
      Zt_DY      <- t(as.matrix(Matrix::bdiag(lapply(1:nb_crop, function(k) z_yxit_list[[k]][t,]))))
      SZt_DX     <- as.matrix(Matrix::bdiag(lapply(1:nb_input, function(b) Zt_DY )))
      mat_yx_tp  <- as.matrix(Matrix::bdiag(Zt_DY, SZt_DX))
    }))


    if(is.null(crop_rp_indvar)){
      zi_D      <- NULL
    }else if(!is.list(crop_rp_indvar)){
      z_bi    <- as.matrix(data[data[,idtime[1]]==hh[i], crop_rp_indvar])
      zi_D <- t(as.matrix(Matrix::bdiag(lapply(1:nb_rp, function(k) c(z_bi[1,]) ))))
    }else if(is.list(crop_rp_indvar)){
      m    <- lapply(1:nb_rp, function(k) as.matrix(data[data[,idtime[1]]==hh[i], crop_rp_indvar[[k]]])[1,])
      zi_D <- t(as.matrix(Matrix::bdiag(lapply(1:nb_rp, function(k) m[[k]]))) )
    }

    if(!is.null(weight)){
      wData   <- as.matrix(data[data[,idtime[1]]==hh[i], weight])
    }else{
      nb_T <- nrow(yit)
      wData   <- rep(1,nb_T)
    }

    if(is.null(crop_subsidy))
      subit <- yit * 0

    res <- list(y      = yit,
                x      = xit,
                sp     = spit,
                s      = shit,
                p      = pit,
                iw     = wit,
                sub    = subit,
                z_yx   = z_yxit,
                mat_Zyx= mat_Zyx,
                Zbar   = zi_D,
                wData  = wData,
                idtim  = idtim)
    return(res)

  })

  ### Initial value for yield equations
  #####################################
  beta_b_0y <- colMeans(as.matrix(NA^(data[,crop_yield]==0)*data[,crop_yield]), na.rm=TRUE)
  beta_b_0x <- lapply(1:nb_input, function(k) colMeans(as.matrix(NA^(data[,crop_input[[k]]]==0)*data[,crop_input[[k]]]), na.rm=TRUE))

  if(opt$distrib_method_beta_yx == "lognormal"){
    beta_b_y    <- log(beta_b_0y)
    beta_b_x   <- log(unlist(beta_b_0x))
    omega_b_y  <- rep(0.05, nb_crop)
    omega_b_x  <- rep(0.05,(nb_crop * nb_input))
  }else if(opt$distrib_method_beta_yx == "normal" ||  opt$distrib_method_beta_yx == "censored-normal"){
    beta_b_y   <- beta_b_0y
    beta_b_x   <- unlist(beta_b_0x)
    omega_b_y  <- rep(5, nb_crop)
    omega_b_x  <- rep(5,(nb_crop * nb_input))
  }

  if(opt$distrib_method_alpha_x == "spherical"){
    beta_b_ax   <- rep(0, nb_crop * nb_input * (nb_input + 1) / 2)
    omega_b_ax  <- rep(0.05, nb_crop * nb_input * (nb_input + 1) / 2)
  }else if(opt$distrib_method_alpha_x == "logchol"){
    beta_b_ax     <- rep(0, nb_crop * nb_input * (nb_input + 1) / 2)
    omega_b_ax  <- rep(0.05, nb_crop * nb_input * (nb_input + 1) / 2)
  }

  var_y <- sapply(1:nb_crop, function(p){
    var_ind <- as.matrix(NA^(data[,crop_yield[p]]==0)*data[,crop_yield[p]]) # transform 0 in NA
    res <- stats::var(var_ind, na.rm = TRUE)
  })

  var_x <- lapply(1:nb_input, function(k){
    var_xx <- sapply(1:nb_crop, function(p){
      var_ind <- as.matrix(NA^(data[,crop_input[[k]][p]]==0)*data[,crop_input[[k]][p]]) # transform 0 in NA
      res <- stats::var(var_ind, na.rm = TRUE)
    })
  })
  var_x  <- unlist(var_x)
  var_yx <- c(var_y,var_x)

  omega_u_yx <- diag(var_yx)

  beta_b  <- as.matrix(c(beta_b_y, beta_b_x, beta_b_ax))
  omega_b <- diag(c(omega_b_y,omega_b_x, omega_b_ax))


  beta_d <- NULL
  if(!is.null(crop_rp_indvar)){
    if(is.list(crop_rp_indvar)){
      beta_d <- matrix(0,nrow = ncol(data_list[[1]]$Zbar))
      rownames(beta_d) <- unlist(crop_rp_indvar)
    }else{
      beta_d <- matrix(0,nrow = ncol(data_list[[1]]$Zbar))
      rownames(beta_d) <- rep(crop_rp_indvar, nb_rp)
    }
  }


  if(is.list(crop_yield_input_indvar)){
    delta_y     <- matrix(0,sum(lengths(crop_yield_input_indvar)),1)#matrix(0,nb_crop*ncol(data_list[[1]]$z_yx),1)
    delta_x     <- lapply(1:nb_input, function(k) matrix(0,sum(lengths(crop_yield_input_indvar)),1))#lapply(1:nb_input, function(k) matrix(0,nrow=nb_crop*ncol(data_list[[1]]$z_yx),ncol=1))
    delta_yx    <- as.matrix(c(delta_y, unlist(delta_x)))
  }else{
    delta_y     <- matrix(0,nb_crop*ncol(data_list[[1]]$z_yx),1)
    delta_x     <- lapply(1:nb_input, function(k) matrix(0,nrow=nb_crop*ncol(data_list[[1]]$z_yx),ncol=1))
    delta_yx    <- as.matrix(c(delta_y, unlist(delta_x)))
  }

  ################################################################################
  row.names(beta_b) <- unlist(beta_b_name)
  #################################################################

  par <- NULL
  par$beta_b     <- beta_b
  par$beta_d     <- beta_d
  par$delta_y    <- delta_y
  par$delta_x    <- delta_x
  par$omega_b    <- omega_b
  par$omega_u_yx <- omega_u_yx


  ## check for par_init argument and update par

  nmsO <- names(par)
  par[(namo <- names(par_init))] <- par_init
  if (length(noNms <- namo[!namo %in% nmsO]))
    stop("unknown names in 'par_init' arguments: ", paste(noNms, collapse = ", "))

  ## check if elements in par have the good dimension


  ##
  par_init   <- par
  ############Step1###########################################
  #invisible(rm(omega.b,omega.e,aa,est_ols))
  invisible(gc())

  est     <- list()
  est$call <- match.call() # get the original call
  #
  cat("\nEstimation Population Parameters step  \n")
  est_pop_tp  <- try(estim_qrp_yx(data_list, par, opt))
  if(inherits(est_pop_tp, "try-error")){
    est_pop <- NULL
    stop( "Problem in estimation   \n" )
  }else{
    est_pop      <- est_pop_tp
    par          <- est_pop$par
    beta_list    <- est_pop$beta_b_saem
    cat("RP calibration step  \n")
    if(opt$calib_method != "estim-sim"){
      est_calib_rp_tp <- try(calib_rp_modyx(data_list, beta_list, par, opt))
      if(inherits(est_calib_rp_tp, "try-error")) {
        beta_b_calib_list <- beta_list
        est_calib    <- NULL
        warning( "Problem in calibration step: last simulation in the estimation is reported \n" )
      }else{
        beta_b_calib_list <- est_calib_rp_tp$beta_b_calib_list
        est_calib         <- est_calib_rp_tp
      }
    }else{
      beta_b_calib_list <- beta_list
    }
  }
  #
  est$beta_b_calib  <- beta_b_calib_list
  est$est_pop       <- est_pop
  est$est_calib     <- est_calib
  est$data_list     <- data_list
  est$opt           <- opt
  est$par_init      <- par_init
  est$namesInmodel  <- namesInmodel
  # Name
  class(est) <- "qrpmultcropxys"
  est
}


