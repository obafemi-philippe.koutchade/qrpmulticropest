#' @title boot_qrp_fact_yxs function
#' @description compute the ss for each individual
#' @importFrom matrixcalc is.positive.definite
#' @importFrom rstan sampling
#' @importFrom rstan extract
#' @importFrom MASS mvrnorm
#' @importFrom LearnBayes dmnorm
#' @importFrom matrixStats colMeans2
#' @importFrom Matrix crossprod
#' @importFrom Matrix head
#' @importFrom Matrix tail
#' @importFrom stats dnorm
#' @importFrom stats pnorm
#' @importFrom dplyr all_of
#' @importFrom dplyr across
#'
#' @param data_list list of
#' @param beta_list list of
#' @param par list of
#' @param opt list of
#' @param temp list of
#' @noRd
boot_qrp_fact_yxs <- function(data_list, beta_list, par, opt, temp) function(i){

  mydata_i    <- data_list[[i]]
  start_value <- beta_list[[i]]
  nbatch      <- opt$estim_rdraw

  s           <- mydata_i$s
  y           <- mydata_i$y
  x           <- Reduce("cbind", mydata_i$x)
  sp          <- mydata_i$sp
  sub         <- mydata_i$sub
  wdata       <- mydata_i$wData
  piw         <- cbind(mydata_i$p, Reduce("cbind", mydata_i$iw))
  sptot       <- as.matrix(rowSums(mydata_i$sp))

  lo_spher_x    <- opt$lo_spher_x
  up_spher_x    <- opt$up_spher_x
  lo_spher_s    <- opt$lo_spher_s
  up_spher_s    <- opt$up_spher_s
  sigma_constr  <- opt$sigma_constr

  distrib_method_beta_yx <- opt$distrib_method_beta_yx
  distrib_method_alpha_x <- opt$distrib_method_alpha_x
  distrib_method_alpha_s <- opt$distrib_method_alpha_s

  distrib_method_beta_yx_stan <- opt$distrib_method_beta_yx_stan
  distrib_method_alpha_x_stan <- opt$distrib_method_alpha_x_stan
  distrib_method_alpha_s_stan <- opt$distrib_method_alpha_s_stan


  nb_input    <- opt$nb_input
  nb_crop     <- opt$nb_crop
  nb_T        <- nrow(mydata_i$y)

  omega_b    <- par$omega_b * temp
  omega_u_yx <- par$omega_u_yx * temp
  omega_u_s  <- par$omega_u_s * temp



  zdelta_yx <- matrix(0, nb_T, nb_crop * ( nb_input + 1))
  for(t in 1:nb_T){
    Zt_DYX   <-  t(as.matrix(Matrix::bdiag(lapply(1:nb_crop, function(k) mydata_i$z_yx[t,][opt$zpos_crop_yx[[k]]]))))
    res_by <- Zt_DYX %*% par$delta_y
    res_bx <- lapply(1:nb_input, function(c) Zt_DYX %*% par$delta_x[[c]])
    zdelta_yx[t, ] <- c(res_by, Reduce("rbind", res_bx))
  }

  zdelta_s <- matrix(0, nb_T, nb_crop - 1)
  for(t in 1:nb_T){
    Zt_DS   <-  t(as.matrix(Matrix::bdiag(lapply(1:(nb_crop - 1), function(k) mydata_i$z_s[t,][opt$zpos_crop_s[[k]]]))))
    res_bs <- Zt_DS %*% par$delta_s
    zdelta_s[t, ] <- c(res_bs)
  }

  ## matZD et par$beta_d à bien specifier
  if(is.null(par$beta_d)){
    m_beta  <- par$beta_b
  }else{
    m_beta  <- par$beta_b + mydata_i$Zbar %*% par$beta_d
  }


  iw_p  <- lapply(1:nb_input, function(k) mydata_i$iw[[k]]/mydata_i$p)
  iw_p  <- Reduce("cbind", iw_p)

  omega_b_diag = diag(diag(omega_b))

  standata     <- list(nb_crop = nb_crop,
                       nb_input = nb_input,
                       start_value = c(start_value),
                       nb_T = nb_T,
                       nb_rp = opt$nb_rp,
                       s = s, x = x, y = y, sp = sp, sub = sub,
                       iw_p = iw_p, piw = piw, wdata = c(wdata),
                       m_beta_b = c(m_beta), omega_b = omega_b,
                       omega_u_yx = omega_u_yx, omega_u_s = omega_u_s,
                       zdelta_yx = zdelta_yx, zdelta_s  = zdelta_s,
                       lo_spher_x = lo_spher_x, up_spher_x = up_spher_x,
                       lo_spher_s = lo_spher_s, up_spher_s = up_spher_s,
                       distrib_method_beta_yx = distrib_method_beta_yx_stan,
                       distrib_method_alpha_x = distrib_method_alpha_x_stan,
                       distrib_method_alpha_s = distrib_method_alpha_s_stan)

  ##############################################################################
  if(opt$sim_method == "map_imh"){

    out_map_tp  <- try(rstan::optimizing(stanmodels$lupost_modyxs_ind,
                                         data = standata, init = "start_value",
                                         hessian = TRUE, as_vector = FALSE),
                                         silent = TRUE)
    if(inherits(out_map_tp,"try-error")){
      m_beta_importance <- c(m_beta)
      omega_importance  <- omega_b
    }else if(out_map_tp$return_code == 0){
      m_beta_importance <- out_map_tp$par$beta_i
      omega_importance_tp <- try(-solve(out_map_tp$hessian), silent = TRUE)
      if(inherits(omega_importance_tp,"try-error")){
        omega_importance <- omega_b
      }else{
        omega_importance <- omega_importance_tp
      }
    }else if(out_map_tp$return_code != 0){
      m_beta_importance <- c(m_beta)
      omega_importance  <- omega_b
    }

    # check if omega_importance is positive definite
    omega_importance <- (omega_importance + t(omega_importance))/2
    if(matrixcalc::is.positive.definite(omega_importance) == FALSE)
      omega_importance <- diag(diag(omega_importance))

    standata_sim_imh <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                         m_beta_importance = m_beta_importance,
                                         omega_importance = omega_importance,
                                         start_value = c(start_value)))

    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                 c(nbatch, opt$nb_rp))
    loglike_c    <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  }else if(opt$sim_method == "nuts"){

    sim_out  <- rstan::sampling(stanmodels$lupost_modyxs_ind, data = standata,
                                chains = opt$estim_chains,
                                warmup = opt$warmup, iter = nbatch + opt$warmup,
                                init = c(start_value), refresh = 0,
                                show_messages = FALSE, verbose = FALSE)
    list_of_draws <- rstan::extract(sim_out)
    sim_i     <- list_of_draws$beta_i
    loglike_c <- mean(list_of_draws$lp__)
  }else if(opt$sim_method == "mhrw"){

    standata_sim_mhrw <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                          start_value = c(start_value),
                                          omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$mhrw_modyxs_ind,
                               standata_sim_mhrw,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"), c(nbatch, opt$nb_rp))
    loglike_c    <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  } else if (opt$sim_method=="marg_imh"){

    standata_sim_imh <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                         start_value = c(start_value),
                                         m_beta_importance = c(m_beta),
                                         omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1,refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                 c(nbatch, opt$nb_rp))
    loglike_c <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  }else if(opt$sim_method == "mhrw_imh"){

    low <- 1
    upper <- nbatch
    nbatch_rw <- low + (upper - low) * stats::plogis(stats::rnorm(1))
    nbatch_rw <- round(nbatch_rw)
    nbatch_ind <- nbatch - nbatch_rw

    standata_sim_mhrw <- c(standata, list(nb_sim = nbatch_rw + opt$nb_burn_sim,
                                          start_value = c(start_value),
                                        omega_importance = diag(diag(omega_b))))
    out_sim <- rstan::sampling(stanmodels$mhrw_modyxs_ind,
                               standata_sim_mhrw,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i_rw        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                    c(nbatch_rw, opt$nb_rp))
    loglike_c_rw    <- Matrix::tail(out_sim_list$log_p_mean, nbatch_rw)

    standata_sim_imh <- c(standata, list(nb_sim = nbatch_ind + opt$nb_burn_sim,
                                         start_value = c(start_value),
                                         m_beta_importance = c(m_beta),
                                         omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1,refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list   <- rstan::extract(out_sim)
    sim_i_ind      <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                   c(nbatch_ind, opt$nb_rp))
    sim_i          <- rbind(sim_i_rw,sim_i_ind)
    loglike_c_ind  <- Matrix::tail(out_sim_list$log_p_mean, nbatch_ind)
    loglike_c      <- mean(c(loglike_c_ind, loglike_c_rw))
  }

  # Sufficient statistic
  ss_rp_1   <- colMeans(sim_i)
  ss_rp_2   <- Matrix::crossprod(sim_i)/nrow(sim_i)

  # ### Computation of information matrix using Ruud approch
  # dup_omega_b   <- matrixcalc::duplication.matrix(opt$nb_rp)
  #
  # solve_omega_b_tp <- try(solve(par$omega_b), silent = TRUE)
  # if(inherits(solve_omega_b_tp, "try-error")) {
  #   solve_omega_b   <- solve(diag(diag(par$omega_b)))
  #   warning( "Problem in standard error step: diag of omega_b is used \n" )
  # }else{
  #   solve_omega_b   <- solve_omega_b_tp
  # }
  #
  # score_beta_b <- 0
  # score_beta_d <- 0
  # score_omega_b <- 0
  # for(r in 1:nrow(sim_i)){
  #   if(is.null(par$beta_d)){
  #     score_beta_b_i  <- solve_omega_b %*% (as.matrix(sim_i[r,]) - par$beta_b)
  #     val_tp  <- tcrossprod(as.matrix(sim_i[r,])) - tcrossprod(as.matrix(sim_i[r,]), par$beta_b) -
  #       tcrossprod(par$beta_b, as.matrix(sim_i[r,])) + tcrossprod(par$beta_b)
  #     score_vech_omega_b_i  <- - 0.5 * t(dup_omega_b) %*%
  #       matrixcalc::vec(solve_omega_b - solve_omega_b %*% val_tp  %*% solve_omega_b)
  #     score_beta_b   <- score_beta_b + score_beta_b_i/nrow(sim_i)
  #     score_beta_d   <- NULL
  #     score_omega_b  <- score_omega_b + score_vech_omega_b_i/nrow(sim_i)
  #   }else{
  #     score_beta_b_i  <- solve_omega_b %*% (as.matrix(sim_i[r,]) - par$beta_b - mydata_i$Zbar %*% par$beta_d)
  #     score_beta_d_i  <- t(mydata_i$Zbar) %*% solve_omega_b %*% (as.matrix(sim_i[r,]) - par$beta_b - mydata_i$Zbar %*% par$beta_d)
  #     val_tp  <- tcrossprod(as.matrix(sim_i[r,])) - tcrossprod(as.matrix(sim_i[r,]), par$beta_b + mydata_i$Zbar %*% par$beta_d) -
  #       tcrossprod(par$beta_b + mydata_i$Zbar %*% par$beta_d, as.matrix(ss_rp_1)) + tcrossprod(par$beta_b + mydata_i$Zbar %*% par$beta_d)
  #     score_vech_omega_b_i  <- - 0.5 * t(dup_omega_b) %*%
  #       matrixcalc::vec(solve_omega_b - solve_omega_b %*% val_tp  %*% solve_omega_b)
  #     score_beta_b   <- score_beta_b + score_beta_b_i/nrow(sim_i)
  #     score_beta_d   <- score_beta_d + score_beta_d_i/nrow(sim_i)
  #     score_omega_b  <- score_omega_b + score_vech_omega_b_i/nrow(sim_i)
  #   }
  # }


  # Sufficient statistic and score delta omega_u_yx
  delta_yx <- c(par$delta_y, unlist(par$delta_x))
  dup_omega_u_yx  <- opt$dup_omega_u_yx
  bigmat_yx <- mydata_i$mat_Zyx
  nb_zyx    <- length(delta_yx)

  if(is.null(par$beta_d)){
    mat_bd <- diag(opt$nb_rp)
    nb_zbd = opt$nb_rp
  }else{
    mat_bd <- cbind(diag(opt$nb_rp), mydata_i$Zbar)
    nb_zbd = opt$nb_rp + nrow(par$beta_d)
  }
  dup_omega_b   <- opt$dup_omega_b

  standata_ssdelta <- c(standata,
                        list(nb_sim = nbatch, beta_i = sim_i,
                             delta_yx = delta_yx,
                             nb_zyx = nb_zyx,
                             bigmat_yx = bigmat_yx,
                             dup_omega_u_yx = dup_omega_u_yx,
                             nb_zbd = nb_zbd,
                             mat_bd = mat_bd,
                             dup_omega_b = dup_omega_b,
                             omega_b = omega_b,
                             m_beta = c(m_beta),
                             score = opt$score_in_est))

  out_ssdelta <- rstan::sampling(stanmodels$ssdelta_modyxs_ind,
                                 standata_ssdelta,
                                 algorithm = "Fixed_param" ,
                                 warmup  = 0, chains = 1, iter = 1,
                                 refresh = 0, show_messages = FALSE,
                                 verbose = FALSE)

  out_ssdelta <- rstan::extract(out_ssdelta)
  ss_rp_3     <- apply(out_ssdelta$ss_rp_yx, 3, rbind)
  ss_rp_4     <- apply(out_ssdelta$ss_rp_yx2, 3, rbind)
  ss_rp_5     <- apply(out_ssdelta$ss_rp_s, 3, rbind)

  score_beta_bd <- apply(out_ssdelta$score_beta_bd, 1, rbind)
  score_omega_b  <- apply(out_ssdelta$score_omega_b, 1, rbind)
  score_delta_yx   <- apply(out_ssdelta$score_delta_yx, 1, rbind)
  score_omega_u_yx <- apply(out_ssdelta$score_omega_u_yx, 1, rbind)


  invisible(gc())
  return(list( ss_rp_1           = ss_rp_1,
               ss_rp_2           = ss_rp_2,
               ss_rp_3           = ss_rp_3,
               ss_rp_4           = ss_rp_4,
               ss_rp_5           = ss_rp_5,
               cloglike          = loglike_c,

               score_beta_bd     =  score_beta_bd,
               score_omega_b     =  score_omega_b,
               score_delta_yx    =  score_delta_yx,
               score_omega_u_yx  =  score_omega_u_yx))
}
#########################################
log_cond_optim_s <- function(par, zs, ss_s, cregime, weight){

  nb_obs <- nrow(zs)
  nb_delta_s <- ncol(zs)
  delta_s    <- as.matrix(par[1:nb_delta_s])
  omega_u_s  <- exp(par[nb_delta_s + 1])
  LogLike <- 0
  for(it in 1:nb_obs){
    omega_u_s_t <- omega_u_s / weight[it]
    u_s <- ss_s[it] - zs[it,] %*% delta_s
    LogLikeS <- cregime[it] * stats::dnorm(x = u_s, sd = sqrt(omega_u_s_t), log = TRUE) + (1 - cregime[it]) * stats::pnorm(q = - u_s, sd = sqrt(omega_u_s_t), log.p = TRUE)
    LogLike  <- LogLike + LogLikeS
  }
  return(LogLike)
}
#################################################
#' @title boot_rp_fact_calib
#' @description `boot_rp_fact_calib_yxs` is used to estimate the standard eror of estimated parameters_
#' @param data_list List of individual data.
#' @param beta_list a list of simulated data obtained at previous iteration_
#' @param par a list of values of parameters of population obtained at current iteration
#' @param opt a list of control parameters. See rpinpall_
#' @return `boot_rp_fact_calib_yxs` returns a list of matrix
#'
boot_rp_fact_calib_yxs <- function(data_list, beta_list, par , opt) function(i) {
  temp        <- 1
  mydata_i    <- data_list[[i]]
  start_value <- as.vector(beta_list[[i]])
  nbatch      <- opt$estim_rdraw

  s           <- mydata_i$s
  y           <- mydata_i$y
  x           <- Reduce("cbind", mydata_i$x)
  sp          <- mydata_i$sp
  sub         <- mydata_i$sub
  wdata       <- mydata_i$wData
  piw         <- cbind(mydata_i$p, Reduce("cbind",mydata_i$iw))
  sptot       <- as.matrix(rowSums(mydata_i$sp))

  lo_spher_x    <- opt$lo_spher_x
  up_spher_x    <- opt$up_spher_x
  lo_spher_s    <- opt$lo_spher_s
  up_spher_s    <- opt$up_spher_s
  sigma_constr  <- opt$sigma_constr

  distrib_method_beta_yx <- opt$distrib_method_beta_yx
  distrib_method_alpha_x <- opt$distrib_method_alpha_x
  distrib_method_alpha_s <- opt$distrib_method_alpha_s

  distrib_method_beta_yx_stan <- opt$distrib_method_beta_yx_stan
  distrib_method_alpha_x_stan <- opt$distrib_method_alpha_x_stan
  distrib_method_alpha_s_stan <- opt$distrib_method_alpha_s_stan


  nb_input    <- opt$nb_input
  nb_crop     <- opt$nb_crop
  nb_T        <- nrow(mydata_i$y)

  omega_b    <- par$omega_b * temp
  omega_u_yx <- par$omega_u_yx * temp
  omega_u_s  <- par$omega_u_s * temp



  zdelta_yx <- matrix(0, nb_T, nb_crop * ( nb_input + 1))
  for(t in 1:nb_T){
    Zt_DYX   <-  t(as.matrix(Matrix::bdiag(lapply(1:nb_crop,
                      function(k) mydata_i$z_yx[t,][opt$zpos_crop_yx[[k]]]))))
    res_by <- Zt_DYX %*% par$delta_y
    res_bx <- lapply(1:nb_input, function(c) Zt_DYX %*% par$delta_x[[c]])
    zdelta_yx[t, ] <- c(res_by, Reduce("rbind", res_bx))
  }

  zdelta_s <- matrix(0, nb_T, nb_crop - 1)
  for(t in 1:nb_T){
    Zt_DS   <-  t(as.matrix(Matrix::bdiag(lapply(1:(nb_crop - 1),
                          function(k) mydata_i$z_s[t,][opt$zpos_crop_s[[k]]]))))
    res_bs <- Zt_DS %*% par$delta_s
    zdelta_s[t, ] <- c(res_bs)
  }

  ## matZD et par$beta_d à bien specifier
  if(is.null(par$beta_d)){
    m_beta  <- par$beta_b
  }else{
    m_beta  <- par$beta_b + mydata_i$Zbar %*% par$beta_d
  }


  iw_p  <- lapply(1:nb_input, function(k) mydata_i$iw[[k]]/mydata_i$p)
  iw_p  <- Reduce("cbind", iw_p)

  omega_b_diag = diag(diag(omega_b))

  standata     <- list(nb_crop = nb_crop,
                       nb_input = nb_input,
                       start_value = c(start_value),
                       nb_T = nb_T,
                       nb_rp = opt$nb_rp,
                       s = s, x = x, y = y, sp = sp, sub = sub,
                       iw_p = iw_p, piw = piw, wdata = c(wdata),
                       m_beta_b = c(m_beta), omega_b = omega_b,
                       omega_u_yx = omega_u_yx, omega_u_s = omega_u_s,
                       zdelta_yx = zdelta_yx, zdelta_s  = zdelta_s,
                       lo_spher_x = lo_spher_x, up_spher_x = up_spher_x,
                       lo_spher_s = lo_spher_s, up_spher_s = up_spher_s,
                       distrib_method_beta_yx = distrib_method_beta_yx_stan,
                       distrib_method_alpha_x = distrib_method_alpha_x_stan,
                       distrib_method_alpha_s = distrib_method_alpha_s_stan)
  #start_value <- c(m_beta)
  if(opt$calib_method == "cmean" |  opt$calib_method == "rscd"){

    sim_out  <- rstan::sampling(stanmodels$lupost_modyxs_ind, data = standata,
                                chains = opt$estim_chains,
                                warmup = opt$warmup, iter = nbatch + opt$warmup,
                                init = c(start_value), refresh = 0,
                                show_messages = FALSE, verbose = FALSE)
    list_of_draws <- rstan::extract(sim_out)
    sim_i   <- list_of_draws$beta_i
    selec   <- (1:opt$calib_chains)*nbatch
    # Conditional mean and variance
    beta_b_RSCD <- colMeans(matrix(sim_i[selec,],nrow = opt$calib_chains))
    beta_b_CM   <- colMeans(sim_i)
    beta_b_CSE  <- sqrt(diag(crossprod(sim_i))/(opt$calib_chains*nbatch))
    beta_b_MAP  <- NULL
    if(opt$calib_method == "rscd"){
      standata$beta_i <- c(beta_b_RSCD)
    }else{
      standata$beta_i <- c(beta_b_CM)
    }

    out_yxs_margetp <- rstan::sampling(stanmodels$calib_yxs_marge_ind,
                                    standata,
                                    algorithm = "Fixed_param" ,
                                    warmup  = 0, chains = 1, iter = 1,
                                    refresh = 0, show_messages = FALSE,
                                    verbose = FALSE)

    out_yxs_marge <- rstan::extract(out_yxs_margetp)
    yx_hat       <- apply(out_yxs_marge$yx_hat, 3, rbind)
    yx_zero_hat  <- apply(out_yxs_marge$yx_zero_hat, 3, rbind)
    marge_hat    <- apply(out_yxs_marge$marge_yx_hat, 3, rbind)
    s_hat        <- apply(out_yxs_marge$s_hat, 3, rbind)
    u_s_hat      <- apply(out_yxs_marge$u_s_hat, 3, rbind)

    beta_yxi_hat <- apply(out_yxs_marge$beta_yxi_v, 3, cbind)
    beta_yxi_hat <- c(beta_yxi_hat)
    beta_axi_hat    <- apply(out_yxs_marge$beta_axi_v, 3, rbind)
    beta_axi_hat <- c(t(beta_axi_hat))
    beta_si_hat    <- c(out_yxs_marge$beta_si_v)
    beta_avsi_hat  <- c(out_yxs_marge$beta_avsi_v)
    beta_aBsi_hat  <- c(out_yxs_marge$beta_aBsi_v)

  }else if(opt$calib_method=="cmode"){

    ## Conditional mode using optimizing of rstan
    out_map  <- rstan::optimizing(stanmodels$lupost_modyxs_ind, data = standata,
                                  init = "start_value",  hessian = TRUE)
    mu_map   <- c(out_map$par[startsWith(names(out_map$par), "beta_i")])
    var_map  <- solve( - out_map$hessian)
    beta_b_MAP    <- mu_map
    beta_b_CM     <- NULL
    beta_b_CSE    <- sqrt(diag(var_map))
    beta_b_RSCD   <- NULL

    standata$beta_i <- beta_b_MAP
    out_yxs_marge <- rstan::sampling(stanmodels$calib_yxs_marge_ind,
                                    standata,
                                    algorithm = "Fixed_param" ,
                                    warmup  = 0, chains = 1, iter = 1,
                                    refresh = 0, show_messages = FALSE,
                                    verbose = FALSE)

    out_yxs_marge <- rstan::extract(out_yxs_marge)
    yx_hat       <- apply(out_yxs_marge$yx_hat, 3, rbind)
    yx_zero_hat  <- apply(out_yxs_marge$yx_zero_hat, 3, rbind)
    marge_hat    <- apply(out_yxs_marge$marge_yx_hat, 3, rbind)
    s_hat        <- apply(out_yxs_marge$s_hat, 3, rbind)
    u_s_hat      <- apply(out_yxs_marge$u_s_hat, 3, rbind)

    beta_yxi_hat <- apply(out_yxs_marge$beta_yxi_v, 3, cbind)
    beta_yxi_hat <- c(beta_yxi_hat)
    beta_axi_hat    <- apply(out_yxs_marge$beta_axi_v, 3, rbind)
    beta_axi_hat <- c(t(beta_axi_hat))
    beta_si_hat    <- c(out_yxs_marge$beta_si_v)
    beta_avsi_hat  <- c(out_yxs_marge$beta_avsi_v)
    beta_aBsi_hat  <- c(out_yxs_marge$beta_aBsi_v)
  }
  return(list(beta_b_MAP = beta_b_MAP,
              beta_b_CM  = beta_b_CM,
              beta_b_CSE = beta_b_CSE ,
              beta_b_RSCD = beta_b_RSCD,
              beta_yxi_hat = beta_yxi_hat,
              beta_axi_hat = beta_axi_hat,
              beta_si_hat = beta_si_hat,
              beta_avsi_hat = beta_avsi_hat,
              beta_aBsi_hat  = beta_aBsi_hat,
              yx_hat       = yx_hat,
              yx_zero_hat  = yx_zero_hat,
              s_hat = s_hat,
              u_s_hat = u_s_hat,
              zdelta_yx = zdelta_yx,
              zdelta_s = zdelta_s,
              marge_hat    = marge_hat))
}

#' @title boot_fact_stde_yxs
#' @description `boot_fact_calib_yx` is used to estimate the standard eror of estimated parameters_
#' @param data_list List of individual data.
#' @param beta_list a list of simulated data obtained at previous iteration_
#' @param par a list of values of parameters of population obtained at current iteration
#' @param opt a list of control parameters. See rpinpall_
#' @return `boot_fact_stde_yxs` returns a list of matrix
#' @noRd
boot_fact_stde_yxs <- function(data_list, beta_list, par , opt) function(i){
  temp        <- 1
  mydata_i    <- data_list[[i]]
  start_value <- beta_list[[i]]
  nbatch      <- opt$stde_rdraw

  s           <- mydata_i$s
  y           <- mydata_i$y
  x           <- Reduce("cbind", mydata_i$x)
  sp          <- mydata_i$sp
  sub         <- mydata_i$sub
  wdata       <- mydata_i$wData
  piw         <- cbind(mydata_i$p, Reduce("cbind", mydata_i$iw))
  sptot       <- as.matrix(rowSums(mydata_i$sp))

  lo_spher_x    <- opt$lo_spher_x
  up_spher_x    <- opt$up_spher_x
  lo_spher_s    <- opt$lo_spher_s
  up_spher_s    <- opt$up_spher_s
  sigma_constr  <- opt$sigma_constr

  distrib_method_beta_yx <- opt$distrib_method_beta_yx
  distrib_method_alpha_x <- opt$distrib_method_alpha_x
  distrib_method_alpha_s <- opt$distrib_method_alpha_s

  distrib_method_beta_yx_stan <- opt$distrib_method_beta_yx_stan
  distrib_method_alpha_x_stan <- opt$distrib_method_alpha_x_stan
  distrib_method_alpha_s_stan <- opt$distrib_method_alpha_s_stan


  nb_input    <- opt$nb_input
  nb_crop     <- opt$nb_crop
  nb_T        <- nrow(mydata_i$y)

  omega_b    <- par$omega_b * temp
  omega_u_yx <- par$omega_u_yx * temp
  omega_u_s  <- par$omega_u_s * temp



  zdelta_yx <- matrix(0, nb_T, nb_crop * ( nb_input + 1))
  for(t in 1:nb_T){
    Zt_DYX   <-  t(as.matrix(Matrix::bdiag(lapply(1:nb_crop, function(k) mydata_i$z_yx[t,][opt$zpos_crop_yx[[k]]]))))
    res_by <- Zt_DYX %*% par$delta_y
    res_bx <- lapply(1:nb_input, function(c) Zt_DYX %*% par$delta_x[[c]])
    zdelta_yx[t, ] <- c(res_by, Reduce("rbind", res_bx))
  }

  zdelta_s <- matrix(0, nb_T, nb_crop - 1)
  for(t in 1:nb_T){
    Zt_DS   <-  t(as.matrix(Matrix::bdiag(lapply(1:(nb_crop - 1), function(k) mydata_i$z_s[t,][opt$zpos_crop_s[[k]]]))))
    res_bs <- Zt_DS %*% par$delta_s
    zdelta_s[t, ] <- c(res_bs)
  }

  ## matZD et par$beta_d à bien specifier
  if(is.null(par$beta_d)){
    m_beta  <- par$beta_b
  }else{
    m_beta  <- par$beta_b + mydata_i$Zbar %*% par$beta_d
  }


  iw_p  <- lapply(1:nb_input, function(k) mydata_i$iw[[k]]/mydata_i$p)
  iw_p  <- Reduce("cbind", iw_p)

  omega_b_diag = diag(diag(omega_b))

  standata     <- list(nb_crop = nb_crop,
                       nb_input = nb_input,
                       start_value = c(start_value),
                       nb_T = nb_T,
                       nb_rp = opt$nb_rp,
                       s = s, x = x, y = y, sp = sp, sub = sub,
                       iw_p = iw_p, piw = piw, wdata = c(wdata),
                       m_beta_b = c(m_beta), omega_b = omega_b,
                       omega_u_yx = omega_u_yx, omega_u_s = omega_u_s,
                       zdelta_yx = zdelta_yx, zdelta_s  = zdelta_s,
                       lo_spher_x = lo_spher_x, up_spher_x = up_spher_x,
                       lo_spher_s = lo_spher_s, up_spher_s = up_spher_s,
                       distrib_method_beta_yx = distrib_method_beta_yx_stan,
                       distrib_method_alpha_x = distrib_method_alpha_x_stan,
                       distrib_method_alpha_s = distrib_method_alpha_s_stan)

  ##############################################################################
  if(opt$sim_method == "map_imh"){

    out_map_tp  <- try(rstan::optimizing(stanmodels$lupost_modyxs_ind,
                                         data = standata, init = "start_value",
                                         hessian = TRUE, as_vector = FALSE),
                       silent = TRUE)
    if(inherits(out_map_tp,"try-error")){
      m_beta_importance <- c(m_beta)
      omega_importance  <- omega_b
    }else if(out_map_tp$return_code == 0){
      m_beta_importance <- out_map_tp$par$beta_i
      omega_importance_tp <- try(-solve(out_map_tp$hessian), silent = TRUE)
      if(inherits(omega_importance_tp,"try-error")){
        omega_importance <- omega_b
      }else{
        omega_importance <- omega_importance_tp
      }
    }else if(out_map_tp$return_code != 0){
      m_beta_importance <- c(m_beta)
      omega_importance  <- omega_b
    }

    # check if omega_importance is positive definite
    omega_importance <- (omega_importance + t(omega_importance))/2
    if(matrixcalc::is.positive.definite(omega_importance) == FALSE)
      omega_importance <- diag(diag(omega_importance))

    standata_sim_imh <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                         m_beta_importance = m_beta_importance,
                                         omega_importance = omega_importance,
                                         start_value = c(start_value)))

    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                 c(nbatch, opt$nb_rp))
    loglike_c    <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  }else if(opt$sim_method == "nuts"){

    sim_out  <- rstan::sampling(stanmodels$lupost_modyxs_ind, data = standata,
                                chains = opt$estim_chains,
                                warmup = opt$warmup, iter = nbatch + opt$warmup,
                                init = c(start_value), refresh = 0,
                                show_messages = FALSE, verbose = FALSE)
    list_of_draws <- rstan::extract(sim_out)
    sim_i     <- list_of_draws$beta_i
    loglike_c <- mean(list_of_draws$lp__)
  }else if(opt$sim_method == "mhrw"){

    standata_sim_mhrw <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                          start_value = c(start_value),
                                          omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$mhrw_modyxs_ind,
                               standata_sim_mhrw,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"), c(nbatch, opt$nb_rp))
    loglike_c    <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  } else if (opt$sim_method=="marg_imh"){

    standata_sim_imh <- c(standata, list(nb_sim = nbatch + opt$nb_burn_sim,
                                         start_value = c(start_value),
                                         m_beta_importance = c(m_beta),
                                         omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1,refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                 c(nbatch, opt$nb_rp))
    loglike_c <- mean(Matrix::tail(out_sim_list$log_p_mean, nbatch))

  }else if(opt$sim_method == "mhrw_imh"){

    low <- 1
    upper <- nbatch
    nbatch_rw <- low + (upper - low) * stats::plogis(stats::rnorm(1))
    nbatch_rw <- round(nbatch_rw)
    nbatch_ind <- nbatch - nbatch_rw

    standata_sim_mhrw <- c(standata, list(nb_sim = nbatch_rw + opt$nb_burn_sim,
                                          start_value = c(start_value),
                                          omega_importance = diag(diag(omega_b))))
    out_sim <- rstan::sampling(stanmodels$mhrw_modyxs_ind,
                               standata_sim_mhrw,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1, refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list <- rstan::extract(out_sim)
    sim_i_rw        <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                    c(nbatch_rw, opt$nb_rp))
    loglike_c_rw    <- Matrix::tail(out_sim_list$log_p_mean, nbatch_rw)

    standata_sim_imh <- c(standata, list(nb_sim = nbatch_ind + opt$nb_burn_sim,
                                         start_value = c(start_value),
                                         m_beta_importance = c(m_beta),
                                         omega_importance = omega_b))
    out_sim <- rstan::sampling(stanmodels$imh_modyxs_ind,
                               standata_sim_imh,
                               algorithm = "Fixed_param" ,
                               warmup  = 0, chains = 1, iter = 1,refresh = 0,
                               show_messages = FALSE, verbose = FALSE)

    out_sim_list   <- rstan::extract(out_sim)
    sim_i_ind      <- Matrix::tail(apply(out_sim_list$beta_accept, 3, "rbind"),
                                   c(nbatch_ind, opt$nb_rp))
    sim_i          <- rbind(sim_i_rw,sim_i_ind)
    loglike_c_ind  <- Matrix::tail(out_sim_list$log_p_mean, nbatch_ind)
    loglike_c      <- mean(c(loglike_c_ind, loglike_c_rw))
  }

  ### Computation of information matrix using Ruud approch
  # Sufficient statistic and score delta omega_u_yx
  delta_yx <- c(par$delta_y, unlist(par$delta_x))
  dup_omega_u_yx  <- opt$dup_omega_u_yx
  bigmat_yx <- mydata_i$mat_Zyx
  nb_zyx    <- length(delta_yx)

  if(is.null(par$beta_d)){
    mat_bd <- diag(opt$nb_rp)
    nb_zbd = opt$nb_rp
  }else{
    mat_bd <- cbind(diag(opt$nb_rp), mydata_i$Zbar)
    nb_zbd = opt$nb_rp + nrow(par$beta_d)
  }
  dup_omega_b   <- opt$dup_omega_b

  standata_ssdelta <- c(standata,
                        list(nb_sim = nbatch, beta_i = sim_i,
                             delta_yx = delta_yx,
                             nb_zyx = nb_zyx,
                             bigmat_yx = bigmat_yx,
                             dup_omega_u_yx = dup_omega_u_yx,
                             nb_zbd = nb_zbd,
                             mat_bd = mat_bd,
                             dup_omega_b = dup_omega_b,
                             omega_b = omega_b,
                             m_beta = c(m_beta),
                             score = 1))

  out_ssdelta <- rstan::sampling(stanmodels$ssdelta_modyxs_ind,
                                 standata_ssdelta,
                                 algorithm = "Fixed_param" ,
                                 warmup  = 0, chains = 1, iter = 1,
                                 refresh = 0, show_messages = FALSE,
                                 verbose = FALSE)

  out_ssdelta <- rstan::extract(out_ssdelta)

  score_beta_bd <- apply(out_ssdelta$score_beta_bd, 1, rbind)
  score_omega_b  <- apply(out_ssdelta$score_omega_b, 1, rbind)
  score_delta_yx   <- apply(out_ssdelta$score_delta_yx, 1, rbind)
  score_omega_u_yx <- apply(out_ssdelta$score_omega_u_yx, 1, rbind)

  return(list(  score_beta_bd      =  score_beta_bd,
                score_omega_b     =  score_omega_b,
                score_delta_yx    =  score_delta_yx,
                score_omega_u_yx  =  score_omega_u_yx))
}

