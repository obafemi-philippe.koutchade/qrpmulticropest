#' Example data
#'
#' An unbalanced panel data
#'
#' @format A data frame with 175 rows and 9 variables:
#' \describe{
#'   \item{id}{individual index}
#'   \item{year}{time index}
#'   \item{y1}{yield level of crop 1 }
#'   \item{y2}{yield level of crop 2 }
#'   \item{y3}{yield level of crop 3 }
#'   \item{x11}{input1 volume of crop 1}
#'   \item{x12}{input1 volume of crop 2}
#'   \item{x13}{input1 volume of crop 3}
#'   \item{x21}{input2 volume of crop 1}
#'   \item{x22}{input2 volume of crop 2}
#'   \item{x23}{input2 volume of crop 3}
#'   \item{s1}{acreage of crop number 1}
#'   \item{s2}{acreage of crop number 2}
#'   \item{s3}{acreage of crop number 3}
#'   \item{k}{Capital}
#'   \item{sau}{total acreage}
#'   \item{temp_mean}{Temperature}
#'   \item{precip}{Annual precipitation}
#'   \item{temp_mean_lag}{Temperature lag}
#'   \item{precip_lag}{Annual precipitation lag}
#'   \item{w11}{input1 price of crop 1}
#'   \item{w12}{input1 price of crop 2}
#'   \item{w13}{input1 price of crop 3}
#'   \item{w21}{input2 price of crop 1}
#'   \item{w22}{input2 price of crop 2}
#'   \item{w23}{input2 price of crop 3}
#'   \item{p1}{yield price of crop 1 }
#'   \item{p2}{yield price of crop 2 }
#'   \item{p3}{yield price of crop 3 }
#'   \item{p1_lag}{yield price lag of crop 1 }
#'   \item{p2_lag}{yield price lag of crop 2 }
#'   \item{p3_lag}{yield price lag of crop 3 }
#'   \item{sub1}{subsidy  of crop 1 }
#'   \item{sub2}{subsidy  of crop 2 }
#'   \item{sub3}{subsidy of crop 3 }
#'   ...
#' }
"my_qrpmulticropest_data"
