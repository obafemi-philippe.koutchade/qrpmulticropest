## Allassonni?re and  Chevallier 2021
#' @title tempering function
#' @description tempering function
#' @param iter_max integer.
#' @param param vector of parameters at iteration n.
#' @return a vector.
tempering_r <- function(iter_max,param=c(0.5,-4,2,4)){

  iter <- 1:iter_max
  kap <- (iter + param[3] * param[4]) / param[4]
  temp <- ifelse(1 + param[1]^kap + param[2] * sin(kap) / kap < 0, 1, 1 + param[1]^kap + param[2] * sin(kap)/kap)

  return(temp)
}

#' @title conv_fun_r function
#' @description conv_fun_r function
#' @param x vector
#' @param y vector
#' @param tol real
conv_fun_r <- function(x, y, tol){

  # Converge function
  # Compute the criteria of convergence, the relative value of estimated value

  #Arguments:
  val <- max(abs((x-y )/(abs(y) + tol)))
  return(val)
}

regime_name_r <- function(x){

  # x is a vector of real
  # This function return a vector of 1 and/or 0
  x    <- c(x)
  val  <- x
  x[x > 0 ]  <- 1
  return(x)
}

mod_rp_fun_beta_xy_r <- function(beta_yx, nb_crop, distrib_method_beta_yx){

  # beta_yx is a vector
  # distrib.method_beta is the distribution of beta_yx
  # This function computes the transformation of beta_y and beta_x and return a matrix with dim (nb_crop, (nb_input + 1))

  if(distrib_method_beta_yx == "lognormal"){
    beta   <- as.matrix(exp(beta_yx))
  } else if(distrib_method_beta_yx == "normal"){
    beta   <- as.matrix(beta_yx)
  } else if(distrib_method_beta_yx == "censored-normal") {
    beta   <- as.matrix(pmax(0, beta_yx))
  }

  m_beta  <- matrix(beta, byrow = FALSE, nrow = nb_crop)

  return(m_beta)
}




mod_rp_fun_alpha_x_r <- function(beta_ax, nb_crop, nb_input, lo_spher, up_spher = pi,distrib_method_alpha_x){

  # g_i is a vector with  dim (nb_crop * nb_input*(nb_input+1)/2)
  # nb_crop and nb_input are integers
  # mod_rp_fun_alpha_x returns a matrix with dim (nb_input*(nb_input+1)/2,nb_crop)
  # distrib_method_alpha_x is the distribution of alpha
  # This function computes the transformation of alpha_x for each crop

  g_i_M   <- matrix(beta_ax, byrow = TRUE, nrow = nb_crop) # transformation of g_i in matrix dim (nb_crop, nb_input*(nb_input+1)/2)

  if(distrib_method_alpha_x == "spherical"){
    m_g_i <- m_g_ax_fun_r(g_i_M,nb_crop, nb_input, lo_spher, up_spher)

  }else if(distrib_method_alpha_x == "logchol"){
    m_g_i <- matrix(0, nb_crop, nb_input*(nb_input+1)/2)
    for(c in 1:nb_crop){
      m_g_i_tp       <- ks::invvech(g_i_M[c,])
      diag(m_g_i_tp) <- exp(diag(m_g_i_tp))
      m_g_i_tp[upper.tri(m_g_i_tp)] <- 0
      m_g_i_tp    <- tcrossprod(m_g_i_tp)
      m_g_i_tp    <- (m_g_i_tp + t(m_g_i_tp))/2
      m_g_i[c,]   <- ks::vech(m_g_i_tp)

    }
  }
  return(m_g_i)
}


m_g_ax_fun_r <- function(g_i_M, nb_crop, nb_input, lo_spher, up_spher){

  # g_i_M is a matrix with dim (nb_crop, nb_input*(nb_input+1)/2)
  # nb_crop and nb_input are integers
  # m_g_ax_fun_r returns a matrix with dim (nb_crop, nb_input*(nb_input+1)/2)
  # This function computes m_g_i for a given g_i matrix using the specified parameters nb_crop, nb_input, lo_spher, and up_spher.

  m_g_i <- matrix(0, nb_crop, nb_input*(nb_input+1)/2)

  for(c in 1:nb_crop){
    d_i <- exp(g_i_M[c,1:nb_input])
    val_ci <- g_i_M[c, (nb_input + 1):(nb_input * (nb_input + 1) / 2)]
    c_i <- lo_spher + (up_spher - lo_spher) * stats::plogis(val_ci)

    list_c <- split(c_i, rep(1:(nb_input - 1), 1:(nb_input - 1)))

    val <- matrix(0,nrow = nb_input, ncol = nb_input)
    val[1,1]  <- d_i[1]

    for (i in 2:nb_input){
      val[i, 1] <- d_i[i]*cos(list_c[[i-1]][1])
      for (j in 2:i){
        if(i == j){
          val[i, j] <- d_i[i] * prod(sin(list_c[[i - 1]][1:(j - 1)]))
        } else {
          val[i, j] <- d_i[i] * prod(sin(list_c[[i - 1]][1:(j - 1)])) * cos(list_c[[i - 1]][j])
        }
      }
    }
    m_g_i_tp  <- Matrix::tcrossprod(val)
    m_g_i_tp  <- (m_g_i_tp + t(m_g_i_tp)) / 2
    m_g_i[c, ] <- ks::vech(m_g_i_tp)
  }
  return(m_g_i)
}



mat_select_reg_obs_r <- function(regime){

  # Selection Matrix of yield or input variable equations: observed
  # regime is a vector
  # This function selects observed yield or input variable equations

  mat_tp     <- diag(1,length(regime))
  val        <- mat_tp[regime > 0,]
  if(sum(regime)==1) val <- matrix(val,nrow = 1)
  return(val)
}


mat_select_reg_mis_r <- function(regime){

  # Selection Matrix of yield or input variable equations: missing
  # regime is a vector
  # This function selects missing yield or input variable equations

  mat_tp     <- diag(1,length(regime))
  val        <- mat_tp[regime == 0,]
  if(sum(regime)== (length(regime)-1)) val <- matrix(val,nrow = 1)
  return(val)
}

# # sit is a vector of acreage share
# # pit
# # yapit
# # xapit
# # profit=pit*yit- xit*wit
# marge.fun <- function(sit,pit,wit,yapit,xapit){
#   nb_crop   <- length(c(sit))
#   marge_vec <- sapply(1:nb_crop, function(c) pit[c]*yapit[c] -  sum(sapply(1:length(wit), function(k) wit[[k]][c]*xapit[[k]][c] )))
#   return(marge_vec)
# }


beta_yxit_fun_r <- function(beta_yxi, zdelta_yxit, nb_crop, nb_input){

  beta_yxit <- beta_yxi + zdelta_yxit

  return(beta_yxit)
}


approx_yxit_fun_r <- function(w_pit, beta_yxit, m_g_it, nb_crop, nb_input){

  # w_pit matrix with dim (nb_input, nb_crop)
  # beta_yit
  # beta_xit
  # m_g_it
  # This function computes the approximation of yit and xit and returns a matrix with dim (nb_crop, (nb_input + 1))

  yxapit <- matrix(0, nb_crop, (nb_input + 1))

  for (c in 1:nb_crop){
    g_i <- ks::invvech(m_g_it[c,])
    yxapit[c, 1] <- beta_yxit[c, 1] - 0.5 * w_pit[c,] %*% g_i %*% as.matrix(w_pit[c,])

    for (i in 1:nb_input){
      yxapit[c,i + 1] <- beta_yxit[c, i + 1] - w_pit[c,] %*% g_i[,i]
    }
  }
  return(yxapit)
}

################################################################################

log_cond_density_allxy_r <- function(s, x, y, iw_p, wdata, omega_u_xy, zdelta_yx, nb_crop, nb_input, lo_spher, up_spher, sigma_constr , distrib_method_beta_yx, distrib_method_alpha_x ) function(beta_i){

  # s is matrix of dim(nb.T, nb_crop)
  # y is matrix of dim(nb.T, nb_crop)
  # x is matrix of dim(nb.T, nb_input)
  # iw_p is matrix of dim(nb.T, nb_crop * nb_input)
  # zdelta_yx is an array with dim (nb_crop, (nb_input + 1), nb.T)
  # omega_u_yx is nb_crop vector
  # nb_crop is an integer
  # nb_input is an integer
  # lo_spher and up_spher are real
  # distrib_method_beta_yx is a charater
  # distrib_method_alpha_x is a character
  # beta_i is a vector
  # This function computes the


  nb.T      <- nrow(y)

  beta_yxi <- beta_i[1:(nb_crop * (nb_input + 1))]
  beta_axi <- beta_i[-c(1:(nb_crop * (nb_input + 1)))]

  beta_yxi_v  <- mod_rp_fun_beta_xy_r(beta_yxi,  nb_crop, distrib_method_beta_yx)
  beta_gi_v   <- mod_rp_fun_alpha_x_r(beta_axi, nb_crop, nb_input, lo_spher, up_spher, distrib_method_alpha_x)

  LogLike <- 0
  for (t in 1:nb.T){
    regimey  <- regime_name_r(s[t, ]) # Regime name
    regimexy <- c(rep(1, nb_input), regimey)

    select_xy_obs  <- mat_select_reg_obs_r(regimexy)  # Selection matrix

    omega_u_xy_t <- omega_u_xy / wdata[t]
    omega_u_xy_obs  <- select_xy_obs %*% omega_u_xy_t %*% t(select_xy_obs)

    zdelta_yxit <- matrix(zdelta_yx[t, ], byrow = FALSE, nrow = nb_crop)

    beta_yxit   <- beta_yxi_v + zdelta_yxit  #beta.yxit.fun(beta_yxi_v, zdelta_yxit, nb_crop, nb_input)



    w_pit  <- t(matrix(iw_p[t,], byrow = TRUE, ncol = nb_crop))
    yxapit <- approx_yxit_fun_r(w_pit,
                              beta_yxit,
                              beta_gi_v,
                              nb_crop,
                              nb_input)

    #Error term y
    u_y     <- as.matrix(y[t,])  - as.matrix(yxapit[, 1])

    # Error term allx
    u_x <- rep(0, nb_input)
    for(i in 1:nb_input){
      u_x[i]   <- x[t,i]  - s[t,] %*% as.matrix(yxapit[, i + 1])
    }


    u_xy_obs <- select_xy_obs %*% as.matrix(c(u_x, u_y))

    log_likelihood_obs <- LearnBayes::dmnorm(x = c(u_xy_obs), varcov = omega_u_xy_obs, log = TRUE)  # LogLikeXY <- - 0.5 * t(u_xy_obs) %*% solve(omega_u_xy_obs) %*%  u_xy_obs - 0.5 * log(det(omega_u_xy_obs))

    # Introduce a penality
    log_likelihood_pen = 0;
    if(sigma_constr > 0){
      vec_yxapit = ks::vec(yxapit)
      pmin_vec_yxapit <- pmin(rep(0.0, nb_crop * (nb_input + 1)), vec_yxapit)
      log_likelihood_pen = LearnBayes::dmnorm(x = c(pmin_vec_yxapit),
                                              varcov = diag(rep(sigma_constr^2, nb_crop * (nb_input + 1))), log = TRUE)
    }else{
      log_likelihood_pen = 0
    }

    #  log likelihood
    LogLike <-  LogLike + log_likelihood_obs + log_likelihood_pen

  }

  return(LogLike)
}

lupost_rp_factory.b_allxy_r  <- function(s, x, y, iw_p, m_beta, omega_b, wdata, omega_u_xy, zdelta_yx, nb_crop, nb_input, lo_spher, up_spher, sigma_constr, distrib_method_beta_yx, distrib_method_alpha_x) function(beta_i) {

  # s is matrix of dim(nb.T, nb_crop)
  # y is matrix of dim(nb.T, nb_crop)
  # x is matrix of dim(nb.T, nb_input)
  # iw_p is matrix of dim(nb.T, nb_crop * nb_input)
  # omega_u_x_T is nb_input vector
  # omega_u_y_T is nb_crop vector
  # nb_crop is an integer
  # nb_input is an integer
  # lo_spher and up_spher are real
  # distrib_method_beta_yx is a charater
  # distrib_method_alpha_x is a character
  # beta_i is a vector

  nb.T <- nrow(y)

  # ##
  LogLikeb <- LearnBayes::dmnorm(x = c(beta_i),
                                 mean   = c(m_beta),
                                 varcov = omega_b,
                                 log    = TRUE)
  ##
  LogLikeyx <- log_cond_density_allxy_r(s, x, y, iw_p, wdata, omega_u_xy,  zdelta_yx, nb_crop, nb_input,
                                        lo_spher, up_spher,sigma_constr, distrib_method_beta_yx, distrib_method_alpha_x)(beta_i)

  ld  <- LogLikeb + LogLikeyx
  return(ld)
}

# s, x, y, iw_p,
# m.beta, R_i, omega_u_x_T, omega_u_y_T,
# nb_crop, nb_input,
# lo_spher = 0, up_spher = pi,
# distrib_method_beta_yx = "lognormal",
# distrib_method_alpha_x = "spherical"

log_cond_density_allxys_r <- function(s,
                                      x,
                                      y,
                                      sp,
                                      iw_p, # iw/p
                                      piw, # cbind(p,iw)
                                      sub,
                                      wdata,
                                      omega_u_xy,
                                      omega_u_s,
                                      zdelta_yx,
                                      zdelta_s,
                                      nb_crop,
                                      nb_input,
                                      lo_spher_x,
                                      up_spher_x,
                                      lo_spher_s,
                                      up_spher_s,
                                      sigma_constr,
                                      distrib_method_beta_yx,
                                      distrib_method_alpha_x,
                                      distrib.method_alpha_s) function(beta_i){

                                        # s is matrix of dim(nb.T, nb_crop)
                                        # y is matrix of dim(nb.T, nb_crop)
                                        # x is matrix of dim(nb.T, nb_input)
                                        # iw_p is matrix of dim(nb.T, nb_crop * nb_input)
                                        # zdelta_yx is an array with dim (nb_crop, (nb_input + 1), nb.T)
                                        # omega_u_yx is nb_crop vector
                                        # nb_crop is an integer
                                        # nb_input is an integer
                                        # lo_spher and up_spher are real
                                        # distrib_method_beta_yx is a charater
                                        # distrib_method_alpha_x is a character
                                        # beta_i is a vector
                                        # This function computes the


                                        nb.T   <- nrow(y)
                                        sptot    <- as.matrix(rowSums(sp))

                                        idx_yx <- (nb_crop * (nb_input + 1))
                                        idx_ax <- idx_yx + (nb_crop * nb_input * (nb_input + 1) / 2)
                                        idx_s  <- idx_ax + nb_crop - 1
                                        idx_sx <- idx_s +  nb_crop * (nb_crop + 1) / 2

                                        beta_yxi <- beta_i[1:idx_yx]
                                        beta_axi <- beta_i[(idx_yx + 1):idx_ax]
                                        beta_si  <- beta_i[(idx_ax + 1):idx_s]
                                        beta_sxi <- beta_i[(idx_s + 1):idx_sx]

                                        # real values
                                        beta_yxi_v   <- mod_rp_fun_beta_xy_r(beta_yx = beta_yxi,  nb_crop = nb_crop, distrib_method_beta_yx = distrib_method_beta_yx)
                                        beta_axi_v   <- mod_rp_fun_alpha_x_r(beta_ax = beta_axi, nb_crop = nb_crop, nb_input = nb_input, lo_spher = lo_spher_x, up_spher = up_spher_x, distrib_method_alpha_x = distrib_method_alpha_x)
                                        beta_si_v    <- beta_si
                                        beta_sxi_v   <- mod_rp_fun_alpha_x_r(beta_sxi, nb_crop = 1, nb_input = nb_crop, lo_spher = lo_spher_s, up_spher = up_spher_s, distrib.method_alpha_s) # attention

                                        LogLike <- 0
                                        for (t in 1:nb.T){
                                          regimey  <- regime_name_r(s[t, ]) # Regime name
                                          regimexy <- c(rep(1, nb_input), regimey)

                                          select_xy_obs  <- mat_select_reg_obs_r(regimexy)  # Selection matrix

                                          omega_u_xy_t <- omega_u_xy / wdata[t]
                                          omega_u_xy_obs  <- select_xy_obs %*% omega_u_xy_t %*% t(select_xy_obs)

                                          zdelta_yxit <- matrix(zdelta_yx[t, ], byrow = FALSE, nrow = nb_crop)
                                          beta_yxit   <- beta_yxi_v + zdelta_yxit  #beta.yxit.fun(beta_yxi_v, zdelta_yxit, nb_crop, nb_input)



                                          w_pit  <- t(matrix(iw_p[t,], byrow = TRUE, ncol = nb_crop))
                                          yxapit <- approx_yxit_fun_r(w_pit = w_pit,
                                                                      beta_yxit = beta_yxit,
                                                                      m_g_it = beta_axi_v,
                                                                      nb_crop = nb_crop,
                                                                      nb_input = nb_input)

                                          #Error term y
                                          u_y     <- as.matrix(y[t,])  - as.matrix(yxapit[, 1])

                                          # Error term allx
                                          u_x <- rep(0, nb_input)
                                          for(i in 1:nb_input){
                                            u_x[i]   <- x[t,i]  - s[t,] %*% as.matrix(yxapit[, i + 1])
                                          }

                                          u_xy_obs <- select_xy_obs %*% as.matrix(c(u_x, u_y))

                                          LogLikeXY <- LearnBayes::dmnorm(x = c(u_xy_obs), varcov = omega_u_xy_obs, log = TRUE)

                                          ############################################################################

                                          #creation de la matrice B et du vecteur v


                                          mat_H <- ks::invvech(beta_sxi_v)
                                          vect_v <- rep(0, nb_crop - 1)
                                          mat_B <- matrix(0, nb_crop - 1, nb_crop - 1)

                                          for(k in 1:(nb_crop - 1)) {
                                            vect_v[k] <- mat_H[k + 1, 1]
                                            for(j in 1:(nb_crop - 1)){
                                              mat_B[k, j] <- mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1])
                                            }
                                          }

                                          ###
                                          zdelta_sit <- zdelta_s[t, ]
                                          beta_sit   <- beta_si_v + zdelta_sit

                                          piwit <- t(matrix(piw[t,], byrow = TRUE, ncol = nb_crop))
                                          res_tp <- yxapit * piwit
                                          marge_yx <- (res_tp[,1] - rowSums(res_tp[,-1])) + sub[t,]
                                          marge_yx_s <- marge_yx[-1] - rep(marge_yx[1], (nb_crop - 1))


                                          regimes       <- regimey[-1]
                                          select_s_obs  <- mat_select_reg_obs_r(regimes)
                                          select_s_mis  <- mat_select_reg_mis_r(regimes)
                                          mat_B_obs     <- select_s_obs %*% mat_B %*% t(select_s_obs)
                                          mat_B_mis_obs <- select_s_mis %*% mat_B %*% t(select_s_obs)


                                          u_s_obs <- select_s_obs %*%  (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs %*% select_s_obs %*% as.matrix(sp[t,-1])
                                          g_s_mis <- select_s_mis %*%  (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs %*% select_s_obs %*% as.matrix(sp[t,-1])

                                          omega_u_s_t <- omega_u_s / wdata[t]
                                          omega_u_s_obs <- select_s_obs %*% omega_u_s_t
                                          omega_u_s_mis <- select_s_mis %*% omega_u_s_t

                                          # jacobian <- as.matrix(Matrix::bdiag(diag(1, sum(regimey)), mat_B_obs))
                                          # res_tp <- - select_s_obs %*% (beta_yxit[,-1][-1,] - matrix(rep(beta_yxit[,-1][1,], nb_crop - 1), byrow = TRUE, nrow = nb_crop - 1 )) / sptot[t]
                                          # jacobian[1:nb_input, (nrow(jacobian) - sum(regimes) + 1):nrow(jacobian)] <- res_tp

                                          LogLikeS <- log(abs(det(mat_B_obs))) + sum(stats::dnorm(x = u_s_obs, sd = sqrt(omega_u_s_obs), log = TRUE)) + sum(stats::pnorm(q = - g_s_mis, sd = sqrt(omega_u_s_mis), log.p = TRUE))


                                          LogLike  <- LogLike + LogLikeXY + LogLikeS
                                        }

                                        return(LogLike)
                                      }

lupost_rp_factory.b_allxys_r  <- function(s, x, y, sp, iw_p, piw, sub,
                                          m_beta, omega_b, wdata,
                                          omega_u_xy, omega_u_s,
                                          zdelta_yx, zdelta_s,
                                          nb_crop, nb_input,
                                          lo_spher_x, up_spher_x,
                                          lo_spher_s, up_spher_s,
                                          sigma_constr,
                                          distrib_method_beta_yx,
                                          distrib_method_alpha_x, distrib.method_alpha_s) function(beta_i) {

                                            # s is matrix of dim(nb.T, nb_crop)
                                            # y is matrix of dim(nb.T, nb_crop)
                                            # x is matrix of dim(nb.T, nb_input)
                                            # iw_p is matrix of dim(nb.T, nb_crop * nb_input)
                                            # omega_u_x_T is nb_input vector
                                            # omega_u_y_T is nb_crop vector
                                            # nb_crop is an integer
                                            # nb_input is an integer
                                            # lo_spher and up_spher are real
                                            # distrib_method_beta_yx is a charater
                                            # distrib_method_alpha_x is a character
                                            # beta_i is a vector

    nb.T <- nrow(y)

    # ##
    LogLikeb <- LearnBayes::dmnorm(x = c(beta_i),
                                   mean   = c(m_beta),
                                   varcov = omega_b,
                                   log    = TRUE)
    ##
    LogLikeyx <- log_cond_density_allxys_r(s, x, y, sp, iw_p, piw, sub, wdata, omega_u_xy, omega_u_s,  zdelta_yx, zdelta_s, nb_crop, nb_input,
                                           lo_spher_x, up_spher_x,
                                           lo_spher_s, up_spher_s,
                                           sigma_constr,
                                           distrib_method_beta_yx, distrib_method_alpha_x, distrib.method_alpha_s)(beta_i)

    ld  <- LogLikeb + LogLikeyx
    return(ld)
  }


estim_delta_xy <- function(data_list, ss_rp_list, opt, omega_u_xy_t, temp ) function(i){
  mydata_i <- data_list[[i]]
  ss_rp_delta_xy <- ss_rp_list[[i]]
  wdata <- mydata_i$wData
  z_yx <- mydata_i$z.yx
  s <- mydata_i$s
  nb_crop <- opt$nb_crop
  nb_input <- opt$nb_input
  zpos_crop_yx <- opt$zpos_crop_yx
  nb_T <- nrow(s)
  sum_1_2 <- lapply(1:nb_T, function(t) {
    Zt_DY      <- t(as.matrix(Matrix::bdiag(lapply(1:nb_crop, function(k) z_yx[t,][zpos_crop_yx[[k]]]))))
    SZt_DX     <- t(as.matrix(Matrix::bdiag(lapply(1:nb_input, function(b) unlist(lapply(1:nb_crop, function(k) s[t,k] * z_yx[t,][zpos_crop_yx[[k]]]))))))
    mat_xy_tp  <- as.matrix(Matrix::bdiag(SZt_DX, Zt_DY))
    omega_u_xy <- omega_u_xy_t * temp

    regimey  <- regime_name_r(s[t, ]) # Regime name
    regimexy <- c(rep(1, nb_input), regimey)

    select_xy_obs  <- mat_select_reg_obs_r(regimexy)  # Selection matrix
    select_xy_mis   <- mat_select_reg_mis_r(regimexy)

    omega_u_xy_obs          <- select_xy_obs %*% omega_u_xy %*% t(select_xy_obs)
    omega_u_xy_obs_mis      <- select_xy_obs %*% omega_u_xy %*% t(select_xy_mis)
    delta_xy_cond_mis_obs   <- t(omega_u_xy_obs_mis) %*% solve(omega_u_xy_obs)
    mat_delta_xy            <- t(select_xy_obs) + t(select_xy_mis) %*% delta_xy_cond_mis_obs

    mat_xy  <- mat_delta_xy  %*% select_xy_obs %*% mat_xy_tp

    sum_1 <- t(mat_xy) %*% solve(omega_u_xy) %*% mat_xy  * wdata[t]
    sum_2 <- t(mat_xy) %*% solve(omega_u_xy) %*% as.matrix(ss_rp_delta_xy[t,])  * wdata[t]
    return(list(sum_1 = sum_1, sum_2 = sum_2))
  })
  sum_1 <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2[[x]][["sum_1"]]))
  sum_2 <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2[[x]][["sum_2"]]))
  return(list(sum_1 = sum_1, sum_2 = sum_2))
}

estim_omega_xy <- function(data_list, ss_rp_list, opt, delta_xy, omega_u_xy_t, temp ) function(i){
  mydata_i <- data_list[[i]]
  ss_rp_delta_xy <- ss_rp_list[[i]]
  wdata <- mydata_i$wData
  z_yx <- mydata_i$z.yx
  s <- mydata_i$s
  nb_crop <- opt$nb_crop
  nb_input <- opt$nb_input
  zpos_crop_yx <- opt$zpos_crop_yx
  nb_T <- nrow(s)
  sum_1_2_3 <- lapply(1:nb_T, function(t){
    Zt_DY      <- t(as.matrix(Matrix::bdiag(lapply(1:nb_crop, function(k) z_yx[t,][zpos_crop_yx[[k]]]))))
    SZt_DX     <- t(as.matrix(Matrix::bdiag(lapply(1:nb_input, function(b) unlist(lapply(1:nb_crop, function(k) s[t,k] * z_yx[t,][zpos_crop_yx[[k]]]))))))
    mat_xy_tp  <- as.matrix(Matrix::bdiag(SZt_DX, Zt_DY))

    omega_u_xy <- omega_u_xy_t * temp

    regimey  <- regime_name_r(s[t, ]) # Regime name
    regimexy <- c(rep(1, nb_input), regimey)

    if(sum(regimey) == length(regimey))
      nb.obs_regcomp <- 1
    else
      nb.obs_regcomp <- 0

    select_xy_obs  <- mat_select_reg_obs_r(regimexy)  # Selection matrix
    select_xy_mis   <- mat_select_reg_mis_r(regimexy)

    omega_u_xy_obs          <- select_xy_obs %*% omega_u_xy %*% t(select_xy_obs)
    omega_u_xy_obs_mis      <- select_xy_obs %*% omega_u_xy %*% t(select_xy_mis)

    omega_u_xy_mis          <- select_xy_mis %*% omega_u_xy %*% t(select_xy_mis)
    omega_u_xy_cond_mix_obs <- omega_u_xy_mis - t(omega_u_xy_obs_mis) %*% solve(omega_u_xy_obs) %*% omega_u_xy_obs_mis

    delta_xy_cond_mis_obs   <- t(omega_u_xy_obs_mis) %*% solve(omega_u_xy_obs)
    mat_delta_xy            <- t(select_xy_obs) + t(select_xy_mis) %*% delta_xy_cond_mis_obs

    mat_xy      <- mat_delta_xy  %*% select_xy_obs %*% mat_xy_tp

    sum_1 <- tcrossprod(mat_xy %*% delta_xy) * wdata[t]
    sum_2 <- tcrossprod(mat_xy %*% delta_xy, as.matrix(ss_rp_delta_xy[t,])) * wdata[t]
    sum_3 <- t(select_xy_mis) %*% omega_u_xy_cond_mix_obs %*% select_xy_mis
    return(list(sum_1 = sum_1, sum_2 = sum_2, sum_3 = sum_3, nb.obs_regcomp = nb.obs_regcomp))
  })
  sum_1 <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2_3[[x]][["sum_1"]]))
  sum_2 <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2_3[[x]][["sum_2"]]))
  sum_3 <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2_3[[x]][["sum_3"]]))
  nb.obs_regcomp <- Reduce("+", lapply(1:nb_T, function(x) sum_1_2_3[[x]][["nb.obs_regcomp"]]))
  return(list(sum_1 = sum_1, sum_2 = sum_2, sum_3 = sum_3, nb.obs_regcomp = nb.obs_regcomp))
}


