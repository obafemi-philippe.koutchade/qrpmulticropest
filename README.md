
<!-- README.md is generated from README.Rmd. Please edit that file -->

# qrpmulticropest

<!-- badges: start -->
<!-- badges: end -->

The goal of qrpmulticropest is to estimate Quadratic Random Parameters
multicrop model described in Koutchade et al., (2024). Joint estimation
of yield sypply equations, demand inputs equations and acreage choice
equations based on SAEM algorithm.

## Installation

You can install the development version of qrpmulticropest like so:

``` r
# install.package(devtools)
# devtools::install_gitlab("obafemi-philippe.koutchade/qrpmulticropest", host = "https://forgemia.inra.fr")
```

## Example

This is a basic example which shows you how to estimate the quadratic
random pararmeters multicrop model:

``` r
## basic example code
rm(list=ls())
library(qrpmulticropest)
library(dplyr)
data(my_qrpmulticropest_data)
data <- my_qrpmulticropest_data
idtime <- c("id","year")
weight <- NULL
crop_yield <- c("y1","y2","y3")
crop_input <- list(x1=c("x11","x12","x13"), x2=c("x21","x22","x23"))
crop_acreage <- c("s1","s2","s3")
crop_price  <- c("p1_lag","p2_lag","p3_lag")
crop_input_price  <- list(w1=c("w11","w12","w13"), w2=c("w21","w22","w23"))
crop_subsidy  <- c("sub1","sub2","sub3")
crop_yield_input_indvar <- list(zyx1=c("k", "temp_mean", "precip"),
                                zyx2=c("k", "temp_mean", "precip"),
                                zyx3=c("k", "temp_mean", "precip"))

crop_acreage_indvar <- list(zs2=c("k", "temp_mean_lag", "precip_lag"),
                            zs3=c("k", "temp_mean_lag", "precip_lag"))
crop_rp_indvar <-  NULL
distrib_method_beta_yx  <-  "lognormal" #c("lognormal","normal","censored-normal")
distrib_method_alpha_x  <- "spherical"  #c("spherical","logchol")
distrib_method_alpha_s  <- "spherical"  #c("spherical","logchol")
sim_method <-  "marg_imh"               #c("mhrw_imh","mhrw", "marg_imh", "map_imh","nuts")
calib_method <-  "cmode"                #c("cmode","cmean")
crop_name <- c("crop1", "crop2", "crop3")

## estimation of yield supply and demand input equations
## distrib_method_alpha_x  <- "spherical" with lo_spher_x = pi/2 and up_spher_x = pi allow complementarity constraint between
## the considered two inputs variables. For unconsraint use lo_spher_x = 0 and 
## up_spher_x = pi or set distrib_method_alpha_x  <- "logchol"
saem_control  <- list(estim_rdraw = 100,
                         lo_spher_x = pi/2, up_spher_x = pi,
                         nb_SA = 10,
                         nb_smooth = 10,
                         nb_burn_saem = 10,
                         nb_burn_sim = 30)
                         
## for parallel implementation use oplan <- future::plan(future::sequential)                         
oplan <- future::plan(future::multisession, workers = 10 )
fit_yx <- rp_model_yx(data,
                       idtime,
                       weight = NULL,
                       crop_yield = crop_yield,
                       crop_input = crop_input,
                       crop_acreage = crop_acreage,
                       crop_price = crop_price,
                       crop_input_price = crop_input_price,
                       crop_subsidy = crop_subsidy,
                       crop_yield_input_indvar = crop_yield_input_indvar,
                       crop_rp_indvar = NULL,
                       distrib_method_beta_yx = distrib_method_beta_yx, 
                       distrib_method_alpha_x = distrib_method_alpha_x, 
                       sim_method = sim_method, 
                       calib_method = calib_method,
                       saem_control = saem_control,
                       par_init = list(),
                       crop_name = crop_name)
on.exit(plan(oplan))

beta_yx <- fit_yx$est_pop$par$beta_b
omega_b_yx <- fit_yx$est_pop$par$omega_b
delta_y <- fit_yx$est_pop$par$delta_y
delta_x <- fit_yx$est_pop$par$delta_x
omega_u_yx <- fit_yx$est_pop$par$omega_u_yx

beta_svsBs <- rep(0, (length(crop_yield) -1) * 2 + (length(crop_yield) -1) * length(crop_yield)/2)
vomega_b_s <- c(rep(5, (length(crop_yield) -1) * 2),
               rep(0.05, (length(crop_yield) -1) * length(crop_yield)/2))

beta_b  <- c(beta_yx, beta_svsBs)
omega_b <- as.matrix(bdiag(omega_b_yx, diag(vomega_b_s)))


par_init <- list(beta_b = beta_b, omega_b = omega_b, delta_y = delta_y,
                 delta_x = delta_x, omega_u_yx = omega_u_yx)



saem_control  <- list(estim_rdraw = 100,
                     lo_spher_x = pi/2, up_spher_x = pi,
                     nb_SA = 200,
                     nb_smooth = 200,
                     nb_burn_saem = 10,
                     nb_burn_sim = 30)
par_init <-  list()
crop_name <- c("crop1", "crop2", "crop3")

## for parallel implementation use oplan <- future::plan(future::sequential)
oplan <- future::plan(future::multisession, workers = 10 )
fit <- qrp_model_yxs(data,
                     idtime,
                     weight = NULL,
                     crop_yield = crop_yield,
                     crop_input = crop_input,
                     crop_acreage = crop_acreage,
                     crop_price = crop_price,
                     crop_input_price = crop_input_price,
                     crop_subsidy = crop_subsidy,
                     crop_yield_input_indvar = crop_yield_input_indvar,
                     crop_acreage_indvar = crop_acreage_indvar,
                     crop_rp_indvar = NULL,
                     distrib_method_beta_yx = distrib_method_beta_yx, 
                     distrib_method_alpha_x = distrib_method_alpha_x, 
                     distrib_method_alpha_s = distrib_method_alpha_s, 
                     sim_method = sim_method, 
                     calib_method = calib_method, 
                     saem_control = saem_control,
                     par_init = par_init,
                     crop_name = crop_name)
on.exit(plan(oplan))
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

``` r
#
```

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date. `devtools::build_readme()` is handy for this.

You can also embed plots, for example:

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub and CRAN.
