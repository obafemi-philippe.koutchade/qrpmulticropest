#include /inst/stan/include/myfunctions.stan
data {

  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  vector[nb_rp] beta_i;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop] sp;
  matrix[nb_T, nb_crop] sub;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_T] wdata;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  matrix[nb_T, nb_crop - 1] zdelta_s;
  real lo_spher_x;
  real up_spher_x;
  real lo_spher_s;
  real up_spher_s;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  int distrib_method_alpha_s;
}

transformed data{
  vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);
  int idx_yx = nb_crop * (nb_input + 1);
  int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1) / 2);
  int idx_s  = idx_ax + nb_crop - 1;
  int idx_avs  = idx_s + nb_crop - 1;
  int idx_aBs = idx_avs +  nb_crop * (nb_crop - 1) / 2;

  vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
  vector[nb_crop * nb_input * (nb_input + 1) / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];
  vector[nb_crop - 1] beta_si = beta_i[(idx_ax + 1):idx_s];
  vector[nb_crop - 1] beta_avsi = beta_i[(idx_s + 1):idx_avs];
  vector[nb_crop * (nb_crop - 1) / 2] beta_aBsi = beta_i[(idx_avs + 1):idx_aBs];
}

model {

}

generated quantities{

  matrix[nb_T, nb_crop * (nb_input + 1)] yx_hat;
  matrix[nb_T, nb_crop * (nb_input + 1)] yx_zero_hat;
  matrix[nb_T, nb_crop] marge_yx_hat;
  matrix[nb_T, nb_crop -1] u_s_hat;
  matrix[nb_T, nb_crop -1] s_hat;

  matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
  matrix[nb_crop, nb_input * (nb_input + 1) / 2] beta_axi_v;
  vector[nb_crop - 1] beta_si_v;
  vector[nb_crop - 1] beta_avsi_v;
  vector[nb_crop * (nb_crop - 1) / 2] beta_aBsi_v;

  // Call the corresponding functions
  beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
  beta_axi_v  = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);
  beta_si_v   = beta_si;
  beta_avsi_v = beta_avsi;
  beta_aBsi_v = to_vector(mod_rp_fun_alpha_x(beta_aBsi, 1, nb_crop - 1, lo_spher_s, up_spher_s, distrib_method_alpha_s));

  for (t in 1:nb_T) {
    /***********************************yield and input *********************/
    // Extract regime name and create regimeyx
    array[nb_crop] int regimey = regime_name(s[t]);
    array[nb_crop + nb_crop * nb_input] int regimeyx;
    int start_l = 1;
    for(l in 1:(nb_input + 1)){
      regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
      start_l = start_l + nb_crop;
    }
    // Call mat_select_reg_obs function
    int N_p_obs = sum(regimeyx);
    int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
    matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

    matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

    // Extract zdelta_yxit for current time step
    matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

    // Compute beta_yxit
    matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

    // Compute yxapit using approx.yxit.fun
    matrix[nb_crop , (nb_input + 1)] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);
    yx_hat[t] = to_vector(yxapit)';
    yx_zero_hat[t] = (select_yx_obs' * select_yx_obs * to_vector(yxapit))';

    // tranformation of beta_v and mat_B
    vector[nb_crop - 1] vect_v = beta_avsi_v;
    matrix[nb_crop - 1, nb_crop - 1] mat_B = vector_to_symmat(beta_aBsi_v, nb_crop-1);

    // zdeltas
    vector[nb_crop - 1] zdelta_sit = zdelta_s[t]';
    vector[nb_crop - 1] beta_sit = beta_si_v + zdelta_sit;

    /*******************************acreage model******************************/
    // Computation of crops' margin
    matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
    matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
    vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;
    vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));

    marge_yx_hat[t] = marge_yx';

    // regime name of acreage
    array[nb_crop - 1] int regime_s ;
    regime_s = regime_name(s[t, 2:nb_crop]);

    // Call mat_select_reg_obs function
    int N_p_obs_s = sum(regime_s);
    int N_p_mis_s = nb_crop - 1  - N_p_obs_s;
    matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
    matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

    matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
    matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


    vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
    vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';
    u_s_hat[t] = (select_s_obs' * u_s_obs + select_s_mis' * g_s_mis)';
    s_hat[t] = (select_s_obs' * inverse(mat_B_obs) * select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]))';
  }

}

