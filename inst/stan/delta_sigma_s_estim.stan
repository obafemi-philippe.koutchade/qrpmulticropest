
data {
  int<lower=0> nb_obs;
  int<lower=0> nb_varx;
  array[nb_obs] int crop_choice;
  vector[nb_obs] weight;
  vector[nb_obs] y;
  matrix[nb_obs, nb_varx] x;
}

parameters {
  vector[nb_varx] mu;
  real sigma;
}

transformed parameters {
  vector[nb_obs] u_s = y - x * mu;
  vector[nb_obs] sigma_it = sqrt(rep_vector(exp(sigma), nb_obs) .* weight);
}


model {
  mu ~ multi_normal(rep_vector(0,nb_varx), 100 * identity_matrix(nb_varx));
  sigma ~ normal(0, 100);
  for(i in 1:nb_obs){
    target += crop_choice[i] * normal_lpdf(u_s[i] | 0, sigma_it[i] ) +
              (1-crop_choice[i]) * normal_lcdf(-u_s[i] | 0, sigma_it[i] );
  }
}

