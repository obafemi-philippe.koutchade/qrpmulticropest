functions{
  real log_dens_delta_sigma_s(int nb_obs, vector y, matrix x, vector mu, real sigma_e,
                             vector weight, array[] int crop_choice){
    real log_pp_obs = 0;
    for(j in 1:nb_obs){
      real sigma_it = sqrt(sigma_e / weight[j]);
      real u_s = y[j] - x[j] * mu;
      if(crop_choice[j] == 1)
        log_pp_obs += normal_lpdf(u_s | 0, sigma_it );
      else
        log_pp_obs += normal_lcdf(- u_s | 0, sigma_it );
    }
    return log_pp_obs;
  }
}
data {
  int<lower=0> nb_obs;
  int<lower=0> nb_varx;
  int<lower=0> nb_sim;
  array[nb_obs] int crop_choice;
  vector[nb_obs] weight;
  vector[nb_obs] y;
  matrix[nb_obs, nb_varx] x;
  vector[nb_varx + 1] start_value;
}

model {

}

generated quantities{

  matrix[nb_sim, nb_varx + 1] beta_accept;
  vector[nb_sim] log_p_mean;
  vector[nb_sim] acceptance_rate = rep_vector(0, nb_sim);
  vector[nb_varx + 1] beta_proposed = start_value;

  real sigma_e = exp(beta_proposed[nb_varx + 1]);
  vector[nb_varx] mu = beta_proposed[1:nb_varx];

  beta_accept[1] = beta_proposed' ;// start value

  real log_pp_obs = log_dens_delta_sigma_s(nb_obs, y, x,  mu,  sigma_e,
                                  weight, crop_choice);
  real c = 1;
  real log_pp = log_pp_obs;
  real p_t;
  real p_p;
  real R;
  real u;
  int nb_accepted = 0;
  p_t = log_pp;
  log_p_mean[1] = log_pp;

  int i = 2;
  while (i <= nb_sim) {

    beta_proposed = multi_normal_rng(beta_accept[i - 1], square(c) * identity_matrix(nb_varx + 1));
    sigma_e = exp(beta_proposed[nb_varx + 1]);
    mu = beta_proposed[1:nb_varx];
    log_pp_obs = log_dens_delta_sigma_s(nb_obs, y, x,  mu,  sigma_e,
                                        weight, crop_choice);
    log_pp = log_pp_obs;
    p_p = log_pp ;
    R   = fmin(1, exp(p_p - p_t));
    u   = uniform_rng(0, 1);
    if (u <= R) {
      // accept proposal
      beta_accept[i] = beta_proposed';
      p_t = p_p;  // save density
      log_p_mean[i]  = log_pp;
      nb_accepted = nb_accepted + 1;
    } else {
      // stay with the current value
      beta_accept[i] = beta_accept[i - 1];
      log_p_mean[i]  = log_p_mean[i - 1];
      p_t = p_t;
    }

    acceptance_rate[i] = (nb_accepted * 1.0) / i;

    if (acceptance_rate[i] < 0.20){
       c = c / 1.1;
    } else if(acceptance_rate[i] > 0.32){
      c = c * 1.1;
    }
    i += 1;
  }
}
