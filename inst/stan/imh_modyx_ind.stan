#include /inst/stan/include/myfunctions.stan

// Performs independence chain Metropolis-Hastings (M-H) sampling
// using an Laplace approximation distributions as the candidate density
data {
  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  int<lower=0> nb_sim;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_rp] m_beta_b;
  matrix[nb_rp, nb_rp] omega_b;
  vector[nb_T] wdata;
  matrix[(nb_input + 1) * nb_crop, (nb_input + 1) * nb_crop] omega_u_yx;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  real lo_spher_x;
  real up_spher_x;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  vector[nb_rp] m_beta_importance;
  matrix[nb_rp, nb_rp] omega_importance;
  vector[nb_rp] start_value;

}

model {

}

generated quantities{

  matrix[nb_sim, nb_rp] beta_accept;
  vector[nb_sim] log_p_mean;
  vector[nb_rp] beta_proposed = start_value;
  beta_accept[1] = beta_proposed' ;// start value

  real log_gg = multi_normal_lpdf(beta_proposed | m_beta_importance, omega_importance);

  real log_pp_obs = dens_modyx(s, x, y, iw_p, piw, wdata,
                               zdelta_yx, omega_u_yx,
                               nb_crop, nb_input, lo_spher_x, up_spher_x,
                               distrib_method_beta_yx, distrib_method_alpha_x ,
                               beta_proposed);
  real log_pp_mix = multi_normal_lpdf(beta_proposed | m_beta_b, omega_b) ;
  real log_pp = log_pp_obs + log_pp_mix;
  real p_t;
  real p_p;
  real R;
  real u;
  p_t = log_pp - log_gg;
  log_p_mean[1] = log_pp;

  int i = 2;
  while (i <= nb_sim) {
    beta_proposed = multi_normal_rng(m_beta_importance, omega_importance);
    log_gg = multi_normal_lpdf(beta_proposed | m_beta_importance, omega_importance);

    log_pp_obs =  dens_modyx(s, x, y, iw_p, piw, wdata,
                               zdelta_yx, omega_u_yx,
                               nb_crop, nb_input, lo_spher_x, up_spher_x,
                               distrib_method_beta_yx, distrib_method_alpha_x ,
                               beta_proposed);

    log_pp_mix = multi_normal_lpdf(beta_proposed | m_beta_b, omega_b) ;
    log_pp = log_pp_obs + log_pp_mix;
    p_p = log_pp - log_gg;
    R   = fmin(1, exp(p_p - p_t));
    u   = uniform_rng(0, 1);
    if (u <= R) {
      // accept proposal
      beta_accept[i] = beta_proposed';
      p_t = p_p;  // save density
      log_p_mean[i]  = log_pp;
    } else {
      // stay with the current value
      beta_accept[i] = beta_accept[i - 1];
      log_p_mean[i]  = log_p_mean[i - 1];
      p_t = p_t;
    }
    i += 1;
  }
}


