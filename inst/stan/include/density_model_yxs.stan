
functions{
  // sp acreages, s acreage share, sub: subsidies
  real dens_modyxs(matrix s,
              matrix x,
              matrix y,
              matrix sp,
              matrix sub,
              matrix iw_p,
              matrix piw,
              vector wdata,
              matrix zdelta_yx,
              matrix zdelta_s,
              matrix omega_u_yx,
              vector omega_u_s,
              int nb_crop,
              int nb_input,
              real lo_spher_x,
              real up_spher_x,
              real lo_spher_s,
              real up_spher_s,
              int distrib_method_beta_yx,
              int distrib_method_alpha_x,
              int distrib_method_alpha_s,
              vector beta_i) {

    int nb_T = rows(y);
    real log_likelihood = 0;
    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);

    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);
    int idx_s  = idx_ax + nb_crop - 1;
    int idx_avs = idx_s + nb_crop - 1;
    int idx_aBs = idx_avs +  nb_crop * (nb_crop - 1)  / 2;

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];
    vector[nb_crop - 1] beta_si = beta_i[(idx_ax + 1):idx_s];
    vector[nb_crop - 1] beta_avsi = beta_i[(idx_s + 1):idx_avs];
    vector[nb_crop * (nb_crop - 1)  / 2] beta_sxi = beta_i[(idx_avs + 1):idx_aBs];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;
    vector[nb_crop - 1] beta_si_v;
    vector[nb_crop - 1] beta_avsi_v;
    vector[nb_crop * (nb_crop - 1)  / 2] beta_aBsi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    beta_si_v   = beta_si;
    beta_avsi_v = beta_avsi;
    beta_sxi_v  = to_vector(mod_rp_fun_alpha_x(beta_sxi, 1, nb_crop - 1, lo_spher_s, up_spher_s, distrib_method_alpha_s));


    for (t in 1:nb_T) {
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx = rep_vector(regimey, nb_input + 1);

      // Call mat_select_reg_obs function
      int N_p = sum(regimeyx);
      matrix[N_p, nb_crop *(nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      // Compute omega_u_yx_obs
      matrix[nb_crop *(nb_input + 1), nb_crop *(nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
      matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


      // Compute error terms u_y and u_x
      matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

      // Compute u_yx_obs
      vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

      // Compute log likelihood using dmnorm function
      real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

      // acreage model
      /* matrix[nb_crop, nb_crop] mat_H = vector_to_symmat(beta_sxi_v, nb_crop);
      vector[nb_crop - 1] vect_v = rep_vector(0, nb_crop - 1);
      matrix[nb_crop - 1, nb_crop - 1] mat_B = rep_matrix(0, nb_crop - 1, nb_crop - 1);

     for(k in 1:(nb_crop - 1)) {
        vect_v[k] = mat_H[k + 1, 1];
        for(j in 1:(nb_crop - 1)){
          mat_B[k, j] = mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1]);
        }
      }*/

      vector[nb_crop - 1] vect_v = beta_avsi_v;
      matrix[nb_crop - 1, nb_crop - 1] mat_B = vector_to_symmat(beta_aBsi_v, nb_crop-1);

      vector[nb_crop - 1] zdelta_sit = zdelta_s[t]';
      vector[nb_crop - 1] beta_sit = beta_si_v + zdelta_sit;

      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;

      vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));

      array[nb_crop - 1] int regime_s ;
      regime_s = regime_name(s[t,2:nb_crop]);


      // Call mat_select_reg_obs function
      int N_p_obs_s = sum(regime_s);
      int N_p_mis_s = nb_crop - 1  - N_p_obs_s;
      matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
      matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

      matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
      matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


      vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
      vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';

      vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;
      vector[N_p_obs_s] omega_u_s_obs = select_s_obs * omega_u_s_t;
      vector[N_p_mis_s] omega_u_s_mis = select_s_mis * omega_u_s_t;


      real log_likelihood_s_obs = 0;
      real log_likelihood_s_prob_mis = 0;
      for(i in 1:N_p_obs_s){
        log_likelihood_s_obs += normal_lpdf(u_s_obs[i] | 0, sqrt(omega_u_s_obs[i]) );
      }
      for(i in 1:N_p_mis_s){
        log_likelihood_s_prob_mis += normal_lcdf(- g_s_mis[i] | 0, sqrt(omega_u_s_mis[i]) );
      }

      real log_likelihood_s = log_determinant(mat_B_obs) + log_likelihood_s_obs + log_likelihood_s_prob_mis;

      // log likelihood
      log_likelihood += log_likelihood_xy_obs + log_likelihood_s;
    }

    return log_likelihood;
  }
}
