functions {

  // Function to convert elements of a vector to regime values (1 or 0)
  //  regime_name takes a vector x as input.
  // N is the size of the input vector.
  // result is a vector of the same size as x that will hold the regime values.
  // The loop iterates over each element of x, checking if it's greater than 0.
  // If an element of x is greater than 0, the corresponding element in result is set to 1; otherwise, it's set to 0.
  // Finally, the function returns the result vector containing regime values.

  array[] int regime_name(row_vector x) {
    int N = size(x);
    array[N] int result;
    for (i in 1:N) {
      if (x[i] > 0) {
        result[i] = 1;
      } else {
        result[i] = 0;
      }
    }
    return result;
  }

  // Function to compute the transformation of beta_x
  matrix mod_rp_fun_beta_x(vector beta_bx, int nb_crop, int nb_input, int distrib_method_beta_x) {

    matrix[nb_crop, nb_input] m_beta;
    if (distrib_method_beta_x == 1) {
      vector[nb_crop * nb_input] beta = exp(beta_bx);
      m_beta = to_matrix(beta, nb_crop, nb_input);

    } else if (distrib_method_beta_x == 2) {
      vector[nb_crop * (nb_input + 1)] beta = beta_bx;
      m_beta = to_matrix(beta, nb_crop, nb_input);

    } else if (distrib_method_beta_x == 3) {
      vector[nb_crop * nb_input] beta; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
      for(i in 1:nb_crop * nb_input){
        beta[i] = fmax(0, beta_bx[i] );
      }
      m_beta = to_matrix(beta, nb_crop, nb_input);
    }

    return m_beta;
  }


  // Function to compute the transformation of beta_y and beta_x
  matrix mod_rp_fun_beta_xy(vector beta_byx, int nb_crop, int nb_input, int distrib_method_beta_yx) {

    matrix[nb_crop, (nb_input + 1)] m_beta;
    if (distrib_method_beta_yx == 1) {
      vector[nb_crop * (nb_input + 1)] beta = exp(beta_byx);
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));

    } else if (distrib_method_beta_yx == 2) {
      vector[nb_crop * (nb_input + 1)] beta = beta_byx;
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));

    } else if (distrib_method_beta_yx == 3) {
      vector[nb_crop * (nb_input + 1)] beta; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
      for(i in 1:nb_crop * (nb_input + 1)){
        beta[i] = fmax(0, beta_byx[i] );
      }
      m_beta = to_matrix(beta, nb_crop, (nb_input + 1));
    }

    // jj


    return m_beta;
  }


  int symmat_size(int n) {
    int sz;
    // This calculates it iteratively because Stan gives a warning
    // with integer division.
    sz = 0;
    for (i in 1:n) {
      sz = sz + i;
    }
    return sz;
  }

  matrix vector_to_symmat(vector x, int n) {
    matrix[n, n] m;
    int k;
    k = 1;
    for (j in 1:n) {
      for (i in 1:j) {
        m[i, j] = x[k];
        if (i != j) {
          m[j, i] = m[i, j];
        }
        k = k + 1;
      }
    }
    return m;
  }
  //
  matrix dupmatrixrowvec(matrix X, array[] int m){
    matrix[sum(m),cols(X)] Xout;
    int next_row = 1;
    for(i in 1:rows(X)){
      if(m[i] < 0) {
        reject("m has to be positive");
      }
      Xout[next_row:(next_row + m[i] - 1)] = rep_matrix(X[i,], m[i]);
      next_row = next_row + m[i];
    }
    return Xout;
  }
  //
  matrix transformdelta(vector delta, int nb_z_x_max, int nb_crop, array[] int dim_crop_zpos_x){
    matrix[nb_z_x_max, nb_crop] mat_out;
    int next_k = 1;
    for(k in 1:nb_crop){
      mat_out[next_k:(next_k + dim_crop_zpos_x[k] - 1), k] = delta[next_k:(next_k + dim_crop_zpos_x[k] - 1)];
    }
    return mat_out;
  }


  vector symmat_to_vector(matrix x) {
    vector[symmat_size(rows(x))] v;
    int k;
    k = 1;
    // if x is m x n symmetric, then this will return
    // only parts of an m x m matrix.
    for (j in 1:rows(x)) {
      for (i in 1:j) {
        v[k] = x[i, j];
        k = k + 1;
      }
    }
    return v;
  }

  // Function to compute m_g_i for a given g_i matrix using specified parameters
  matrix m_g_ax_fun(matrix g_i_M, int nb_crop, int nb_input, real lo_spher, real up_spher) {

    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] m_g_i;

    // Iterate over each crop
    for (c in 1:nb_crop) {
      // Extract relevant components from g_i_M
      vector[nb_input] d_i = exp(g_i_M[c, 1:nb_input]');
      vector[(nb_input - 1) * nb_input  / 2] val_ci = g_i_M[c, (nb_input + 1):cols(g_i_M)]';

      // Compute c_i
      vector[(nb_input - 1) * nb_input  / 2] c_i = rep_vector(lo_spher, (nb_input - 1) * nb_input  / 2) + (up_spher - lo_spher) * inv_logit(val_ci);

      // Split c_i into groups
     // vector[nb_input - 1] list_c[nb_input - 1];
      array[nb_input - 1] vector[nb_input - 1] list_c;
      for (i in 1:(nb_input - 1)) {
        list_c[i] = c_i[((i - 1) * i  / 2 + 1):(i * (i + 1)  / 2)];
      }

      // Initialize val matrix
      matrix[nb_input, nb_input] val = rep_matrix(0, nb_input, nb_input);
      val[1, 1] = d_i[1];

      // Compute val matrix elements
      for (i in 2:nb_input) {
        val[i, 1] = d_i[i] * cos(list_c[i - 1][1]);
        for (j in 2:i) {
          if (i == j) {
            val[i, j] = d_i[i] * prod(sin(list_c[i - 1][1:(j - 1)]));
          } else {
            val[i, j] = d_i[i] * prod(sin(list_c[i - 1][1:(j - 1)])) * cos(list_c[i - 1][j]);
          }
        }
      }

      // Compute m_g_i_tp
      matrix[nb_input, nb_input] m_g_i_tp = tcrossprod(val); //val * val';
      m_g_i_tp = (m_g_i_tp + m_g_i_tp') / 2.0;  // Symmetrize

      // Store m_g_i_tp as a vector
      m_g_i[c] = symmat_to_vector(m_g_i_tp)';
    }

    return m_g_i;
  }

  // Function to compute the transformation of alpha_x for each crop
  matrix mod_rp_fun_alpha_x(vector beta_ax, int nb_crop, int nb_input, real lo_spher, real up_spher, int distrib_method_alpha) {

    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] m_g_i;
    matrix[nb_input, nb_input] m_g_i_tp;
    matrix[nb_input, nb_input] result;

    // Transform g_i vector into a matrix
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] g_i_M = to_matrix(beta_ax, nb_crop, nb_input * (nb_input + 1)  / 2, 0);

    if (distrib_method_alpha == 1) {
      // Compute m_g_i using m.g.ax.fun
        m_g_i = m_g_ax_fun(g_i_M, nb_crop, nb_input, lo_spher, up_spher);
    } else if (distrib_method_alpha == 2) {
      // Compute m_g_i using inverse vech transformation
      for (c in 1:nb_crop) {
        m_g_i_tp = vector_to_symmat( to_vector(g_i_M[c]), nb_input);

        for (i in 1:nb_input) {
          for (j in 1:nb_input) {
            if (i == j) {
               result[i, j] = exp(m_g_i_tp[i, j]);
            } else if (i < j){
               result[i, j] = 0;
            } else{
               result[i, j] = m_g_i_tp[i, j];
            }
          }
        }

        m_g_i_tp =  tcrossprod(result); //result * result';
        m_g_i_tp =  (m_g_i_tp + m_g_i_tp') / 2.0;
        m_g_i[c] = symmat_to_vector(m_g_i_tp)';
      }
    }

    return m_g_i;
  }


  // Selection Matrix of yield or input variable equations: observed
  matrix mat_select_reg_obs(array[] int regime) {
    int N = size(regime);
    int N_p = sum(regime);
    matrix[N, N] mat_tp = diag_matrix(rep_vector(1, N));
    matrix[N_p, N] val;

    val = rep_matrix(0, N_p, N); // Initialize val with zeros

    int idx = 1;
    for (i in 1:N) {
      if (regime[i] > 0) {
        val[idx] = mat_tp[i];
        idx += 1;
      }
    }
    return val;
  }

  // Selection Matrix of yield or input variable equations: missing
  matrix mat_select_reg_mis(array[] int regime) {
    int N = size(regime);
    int N_p = N - sum(regime);
    matrix[N, N] mat_tp = diag_matrix(rep_vector(1, N));
    matrix[N_p, N] val;

    val = rep_matrix(0, N_p, N); // Initialize val with zeros

    int idx = 1;
    for (i in 1:N) {
      if (regime[i] == 0) {
        val[idx] = mat_tp[i];
        idx += 1;
      }
    }
    return val;
  }

  //
  matrix approx_yxit_fun(matrix w_pit, matrix beta_yxit, matrix m_g_it, int nb_crop, int nb_input) {
    matrix[nb_crop, nb_input + 1] yxapit;

    for (c in 1:nb_crop) {
      matrix[nb_input , nb_input] g_i = vector_to_symmat(m_g_it[c]', nb_input) ;  //
      yxapit[c, 1] = beta_yxit[c, 1] - 0.5 * dot_product(w_pit[c], g_i * w_pit[c]')  ; //w_pit[c] * g_i * w_pit[c]'

      for (j in 1:nb_input) {
        yxapit[c, j + 1] = beta_yxit[c, j + 1] - dot_product(w_pit[c], g_i[j]') ;
      }
    }
    return yxapit;
  }

  real  modallxy(matrix s,
                      matrix x,
                      matrix y,
                      matrix iw_p,
                      vector wdata,
                      matrix zdelta_yx,
                      matrix omega_u_xy,
                      int nb_crop,
                      int nb_input,
                      real lo_spher_x,
                      real up_spher_x,
                      real sigma_constr,
                      int distrib_method_beta_yx,
                      int distrib_method_alpha_x,
                      vector beta_i) {
    int nb_T = rows(y);
    real log_likelihood = 0;

    vector[nb_crop * (nb_input + 1)] beta_yxi = head(beta_i, nb_crop * (nb_input + 1));
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = tail(beta_i, nb_crop * nb_input * (nb_input + 1)  / 2);

    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    for (t in 1:nb_T) {
      // Extract regime name and create regimexy
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_input] int regimexy = append_array(rep_array(1, nb_input), regimey);

      // Call mat_select_reg_obs function
      int N_p = sum(regimexy);
      matrix[N_p, nb_crop + nb_input] select_xy_obs = mat_select_reg_obs(regimexy);

      // Compute omega_u_xy_obs
      matrix[nb_crop + nb_input, nb_crop + nb_input] omega_u_xy_t = omega_u_xy / wdata[t];
      matrix[N_p, N_p] omega_u_xy_obs = quad_form(omega_u_xy_t, select_xy_obs') ; //select_xy_obs * omega_u_xy_t * select_xy_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


      // Compute error terms u_y and u_x
      vector[nb_input] u_x;
      vector[nb_crop] u_y = to_vector(y[t]) - yxapit[, 1];
      for (i in 1:nb_input) {
        u_x[i] = x[t, i] - dot_product(s[t], yxapit[, i + 1]);
      }

      // Compute u_xy_obs
      vector[N_p] u_xy_obs = select_xy_obs * append_row(u_x, u_y);

      // Compute log likelihood using dmnorm function
      real log_likelihood_obs = multi_normal_lpdf(u_xy_obs | rep_vector(0, N_p), omega_u_xy_obs);

      // Introduce a penality
      real log_likelihood_pen;
      if(sigma_constr > 0){
        vector[nb_crop * (nb_input + 1)] vec_yxapit = to_vector(yxapit);

        vector[nb_crop * (nb_input + 1)] pmin_vec_yxapit; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
        for(i in 1:(nb_crop * (nb_input + 1))){
          pmin_vec_yxapit[i] = fmin(0, vec_yxapit[i]);
        }
        log_likelihood_pen = multi_normal_lpdf(pmin_vec_yxapit | rep_vector(0, nb_crop * (nb_input + 1)), diag_matrix(rep_vector(sigma_constr * sigma_constr, nb_crop * (nb_input + 1))));
      }else{
        log_likelihood_pen = 0;
      }

      // log likelihood
      log_likelihood += log_likelihood_obs + log_likelihood_pen;
    }

    return log_likelihood;
  }

  real  modallxy_old(matrix s,
                      matrix x,
                      matrix y,
                      matrix iw_p,
                      vector wdata,
                      matrix zdelta_yx,
                      matrix omega_u_xy,
                      int nb_crop,
                      int nb_input,
                      real lo_spher,
                      real up_spher,
                      int distrib_method_beta_yx,
                      int distrib_method_alpha_x,
                      vector beta_i) {
    int nb_T = rows(y);
    real log_likelihood = 0;

    vector[nb_crop * (nb_input + 1)] beta_yxi = head(beta_i, nb_crop * (nb_input + 1));
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = tail(beta_i, nb_crop * nb_input * (nb_input + 1)  / 2);

    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher, up_spher, distrib_method_alpha_x);

    for (t in 1:nb_T) {
      // Extract regime name and create regimexy
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_input] int regimexy = append_array(rep_array(1, nb_input), regimey);

      // Call mat_select_reg_obs function
      int N_p = sum(regimexy);
      matrix[N_p, nb_crop + nb_input] select_xy_obs = mat_select_reg_obs(regimexy);

      // Compute omega_u_xy_obs
      matrix[nb_crop + nb_input, nb_crop + nb_input] omega_u_xy_t = omega_u_xy / wdata[t];
      matrix[N_p, N_p] omega_u_xy_obs = select_xy_obs * omega_u_xy_t * select_xy_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

       matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);
      // Compute error terms u_y and u_x
      vector[nb_input] u_x;
      vector[nb_crop] u_y = to_vector(y[t]) - yxapit[, 1];
      for (i in 1:nb_input) {
        u_x[i] = x[t, i] - dot_product(s[t], yxapit[, i + 1]);
      }

      // Compute u_xy_obs
      vector[N_p] u_xy_obs = select_xy_obs * append_row(u_x, u_y);

      // Compute log likelihood using dmnorm function
      log_likelihood += multi_normal_lpdf(u_xy_obs | rep_vector(0, N_p), omega_u_xy_obs);
      //log_likelihood += - 0.5 * u_xy_obs' * inverse(omega_u_xy_obs) * u_xy_obs - 0.5 * log_determinant(omega_u_xy_obs) ;

    }

    return log_likelihood;
  }

  // sp acreages, s acreage share, sub: subsidies
  real  modallxys(matrix s,
                  matrix x,
                  matrix y,
                  matrix sp,
                  matrix sub,
                  matrix iw_p,
                  matrix piw,
                  vector wdata,
                  matrix zdelta_yx,
                  matrix zdelta_s,
                  matrix omega_u_xy,
                  vector omega_u_s,
                  int nb_crop,
                  int nb_input,
                  real lo_spher_x,
                  real up_spher_x,
                  real lo_spher_s,
                  real up_spher_s,
                  real sigma_constr,
                  int distrib_method_beta_yx,
                  int distrib_method_alpha_x,
                  int distrib_method_alpha_s,
                  vector beta_i) {

    int nb_T = rows(y);
    real log_likelihood = 0;
    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);

    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);
    int idx_s  = idx_ax + nb_crop - 1;
    int idx_sx = idx_s +  nb_crop * (nb_crop + 1)  / 2;

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];
    vector[nb_crop - 1] beta_si = beta_i[(idx_ax + 1):idx_s];
    vector[nb_crop * (nb_crop + 1)  / 2] beta_sxi = beta_i[(idx_s + 1):idx_sx];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;
    vector[nb_crop - 1] beta_si_v;
    vector[nb_crop * (nb_crop + 1)  / 2] beta_sxi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    beta_si_v   = beta_si;
    beta_sxi_v  = to_vector(mod_rp_fun_alpha_x(beta_sxi, 1, nb_crop, lo_spher_s, up_spher_s, distrib_method_alpha_s));


    for (t in 1:nb_T) {
      // Extract regime name and create regimexy
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_input] int regimexy = append_array(rep_array(1, nb_input), regimey);

      // Call mat_select_reg_obs function
      int N_p = sum(regimexy);
      matrix[N_p, nb_crop + nb_input] select_xy_obs = mat_select_reg_obs(regimexy);

      // Compute omega_u_xy_obs
      matrix[nb_crop + nb_input, nb_crop + nb_input] omega_u_xy_t = omega_u_xy / wdata[t];
      matrix[N_p, N_p] omega_u_xy_obs = quad_form(omega_u_xy_t, select_xy_obs') ;//select_xy_obs * omega_u_xy_t * select_xy_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


      // Compute error terms u_y and u_x
      vector[nb_input] u_x;
      vector[nb_crop] u_y = to_vector(y[t]) - yxapit[, 1];
      for (i in 1:nb_input) {
        u_x[i] = x[t, i] - dot_product(s[t], yxapit[, i + 1]);
      }

      // Compute u_xy_obs
      vector[N_p] u_xy_obs = select_xy_obs * append_row(u_x, u_y);

      // Compute log likelihood using dmnorm function
      real log_likelihood_xy_obs = multi_normal_lpdf(u_xy_obs | rep_vector(0, N_p), omega_u_xy_obs);

      // Introduce a penality for xy
      real log_likelihood_xy_pen = 0;
      if(sigma_constr > 0){
        vector[nb_crop * (nb_input + 1)] vec_yxapit = to_vector(yxapit);
        vector[nb_crop * (nb_input + 1)] pmin_vec_yxapit;
        for(i in 1:(nb_crop * (nb_input + 1))){
          pmin_vec_yxapit[i] = fmin(0, vec_yxapit[i]);
          log_likelihood_xy_pen += normal_lpdf(pmin_vec_yxapit[i] |0, sigma_constr);
        }
      }else{
        log_likelihood_xy_pen = 0;
      }

      // acreage model
      matrix[nb_crop, nb_crop] mat_H = vector_to_symmat(beta_sxi_v, nb_crop);
      vector[nb_crop - 1] vect_v = rep_vector(0, nb_crop - 1);
      matrix[nb_crop - 1, nb_crop - 1] mat_B = rep_matrix(0, nb_crop - 1, nb_crop - 1);

      for(k in 1:(nb_crop - 1)) {
        vect_v[k] = mat_H[k + 1, 1];
        for(j in 1:(nb_crop - 1)){
          mat_B[k, j] = mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1]);
        }
      }

      vector[nb_crop - 1] zdelta_sit = zdelta_s[t]';
      vector[nb_crop - 1] beta_sit = beta_si_v + zdelta_sit;

      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;

      vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));

      array[nb_crop - 1] int regime_s ;
      //regime_s = regimey[2:nb_crop];
      regime_s = regime_name(s[t,2:nb_crop]);


      // Call mat_select_reg_obs function
      int N_p_obs_s = sum(regime_s);
      int N_p_mis_s = nb_crop - 1  - N_p_obs_s;
      matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
      matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

      matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
      matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


      vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
      vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';

      vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;
      vector[N_p_obs_s] omega_u_s_obs = select_s_obs * omega_u_s_t;
      vector[N_p_mis_s] omega_u_s_mis = select_s_mis * omega_u_s_t;


      real log_likelihood_s_obs = 0;
      real log_likelihood_s_prob_mis = 0;
      for(i in 1:N_p_obs_s){
        log_likelihood_s_obs += normal_lpdf(u_s_obs[i] | 0, sqrt(omega_u_s_obs[i]) );
      }
      for(i in 1:N_p_mis_s){
        log_likelihood_s_obs += normal_lcdf(- g_s_mis[i] | 0, sqrt(omega_u_s_mis[i]) );
      }

      // Penality for s >0
      real log_likelihood_s_pen = 0;
      if(sigma_constr > 0){
        vector[N_p_obs_s] vec_sapit = mdivide_left_spd(mat_B_obs, to_vector(select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t])));
        vector[N_p_obs_s] pmin_vec_sapit;
        for(i in 1:N_p_obs_s){
          pmin_vec_sapit[i] = fmin(0, vec_sapit[i]);
          log_likelihood_s_pen += normal_lpdf(pmin_vec_sapit[i] |0, sigma_constr);
        }
      }else{
        log_likelihood_s_pen = 0;
      }

      real log_likelihood_s = log_determinant(mat_B_obs) + log_likelihood_s_obs + log_likelihood_s_prob_mis;


      // log likelihood
      log_likelihood += log_likelihood_xy_obs + log_likelihood_xy_pen + log_likelihood_s + log_likelihood_s_pen;
    }

    return log_likelihood;
  }



 // sp acreages, s acreage share, sub: subsidies
  real  modalls(matrix s,
                  matrix sp,
                  matrix sub,
                  matrix iw_p,
                  matrix piw,
                  vector wdata,
                  matrix zdelta_yx,
                  matrix zdelta_s,
                  vector omega_u_s,
                  int nb_crop,
                  int nb_input,
                  real lo_spher_x,
                  real up_spher_x,
                  real lo_spher_s,
                  real up_spher_s,
                  real sigma_constr,
                  int distrib_method_beta_yx,
                  int distrib_method_alpha_x,
                  int distrib_method_alpha_s,
                  vector beta_xyaxi,vector beta_i) {

    int nb_T = rows(s);
    real log_likelihood = 0;
    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);

    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);
    int idx_s  = nb_crop - 1;
    int idx_sx = idx_s +  nb_crop * (nb_crop + 1)  / 2;

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_xyaxi[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_xyaxi[(idx_yx + 1):idx_ax];
    vector[nb_crop - 1] beta_si = beta_i[1:idx_s];
    vector[nb_crop * (nb_crop + 1)  / 2] beta_sxi = beta_i[(idx_s + 1):idx_sx];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;
    vector[nb_crop - 1] beta_si_v;
    vector[nb_crop * (nb_crop + 1)  / 2] beta_sxi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    beta_si_v   = beta_si;
    beta_sxi_v  = to_vector(mod_rp_fun_alpha_x(beta_sxi, 1, nb_crop, lo_spher_s, up_spher_s, distrib_method_alpha_s));


    for (t in 1:nb_T) {

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);



      // acreage model
      matrix[nb_crop, nb_crop] mat_H = vector_to_symmat(beta_sxi_v, nb_crop);
      vector[nb_crop - 1] vect_v = rep_vector(0, nb_crop - 1);
      matrix[nb_crop - 1, nb_crop - 1] mat_B = rep_matrix(0, nb_crop - 1, nb_crop - 1);

      for(k in 1:(nb_crop - 1)) {
        vect_v[k] = mat_H[k + 1, 1];
        for(j in 1:(nb_crop - 1)){
          mat_B[k, j] = mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1]);
        }
      }

      vector[nb_crop - 1] zdelta_sit = zdelta_s[t]';
      vector[nb_crop - 1] beta_sit = beta_si_v + zdelta_sit;

      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;

      vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));

      array[nb_crop - 1] int regime_s ;
      //regime_s = regimey[2:nb_crop];
      regime_s = regime_name(s[t,2:nb_crop]);


      // Call mat_select_reg_obs function
      int N_p_obs_s = sum(regime_s);
      int N_p_mis_s = nb_crop - 1  - N_p_obs_s;
      matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
      matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

      matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
      matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


      vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
      vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';

      vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;
      vector[N_p_obs_s] omega_u_s_obs = select_s_obs * omega_u_s_t;
      vector[N_p_mis_s] omega_u_s_mis = select_s_mis * omega_u_s_t;


      real log_likelihood_s_obs = 0;
      real log_likelihood_s_prob_mis = 0;
      for(i in 1:N_p_obs_s){
        log_likelihood_s_obs += normal_lpdf(u_s_obs[i] | 0, sqrt(omega_u_s_obs[i]) );
      }
      for(i in 1:N_p_mis_s){
        log_likelihood_s_obs += normal_lcdf(- g_s_mis[i] | 0, sqrt(omega_u_s_mis[i]) );
      }

      // Penality for s >0
      real log_likelihood_s_pen = 0;
      if(sigma_constr > 0){
        vector[N_p_obs_s] vec_sapit = mdivide_left_spd(mat_B_obs, to_vector(select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t])));
        vector[N_p_obs_s] pmin_vec_sapit;
        for(i in 1:N_p_obs_s){
          pmin_vec_sapit[i] = fmin(0, vec_sapit[i]);
          log_likelihood_s_pen += normal_lpdf(pmin_vec_sapit[i] |0, sigma_constr);
        }
      }else{
        log_likelihood_s_pen = 0;
      }

      real log_likelihood_s = log_determinant(mat_B_obs) + log_likelihood_s_obs + log_likelihood_s_prob_mis;


      // log likelihood
      log_likelihood += log_likelihood_s + log_likelihood_s_pen;
    }

    return log_likelihood;
  }



  real cmodallxy(int nb_crop,
              int nb_input,
              int nb_id,
              int nb_z_xy_max,
              int nb_delta_y,
              int nb_rp_yx,
              matrix mat_s,
              matrix mat_x,
              matrix mat_y,
              matrix mat_iw_p,
              vector mat_wdata,
              matrix mat_z_xy,
              array[] int dim_it,
              array[,] int mat_crop_zpos_xy,
              array[] int dim_crop_zpos_xy,
              matrix omega_u_xy,
              real lo_spher_x,
              real up_spher_x,
              real sigma_constr,
              int distrib_method_beta_yx,
              int distrib_method_alpha_x,
              vector delta_y,
              matrix delta_x,
              matrix mat_beta_i) {

    //int nb_rp_yx = cols[mat_beta_i];
   // int nb_z_xy_max = cols(mat_z_xy);
    //int nb_crop  = cols(mat_s);
    //int nb_input = cols(mat_x);
    //int nb_id = rows(mat_beta_i)

    real log_likelihood = 0;

    int start_ind = 1;
    int end_ind = dim_it[1];
    for(i in 1:nb_id){
      int nb_T = dim_it[i];
      matrix[nb_T, nb_crop] s = mat_s[start_ind:end_ind];
      matrix[nb_T, nb_crop] y = mat_y[start_ind:end_ind];
      matrix[nb_T, nb_input] x = mat_x[start_ind:end_ind];
      vector[nb_T] wdata = mat_wdata[start_ind:end_ind];
      matrix[nb_T, nb_z_xy_max] z_xy = mat_z_xy[start_ind:end_ind];
      matrix[nb_T, nb_crop * nb_input] iw_p = mat_iw_p[start_ind:end_ind];

      start_ind = start_ind + dim_it[i];
      if(i < nb_id)
        end_ind = end_ind + dim_it[i + 1];

      //
      vector[nb_rp_yx] beta_i = mat_beta_i[i]';
      vector[nb_crop * (nb_input + 1)] beta_yxi = head(beta_i, nb_crop * (nb_input + 1));
      vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = tail(beta_i, nb_crop * nb_input * (nb_input + 1)  / 2);

      matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
      matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;

      // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
      beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
      beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

      for (t in 1:nb_T) {

        // Extract regime name and create regimexy
        array[nb_crop] int regimey = regime_name(s[t]);
        array[nb_crop + nb_input] int regimexy = append_array(rep_array(1, nb_input), regimey);

        // Call mat_select_reg_obs function
        int N_p = sum(regimexy);
        matrix[N_p, nb_crop + nb_input] select_xy_obs = mat_select_reg_obs(regimexy);

        // Compute omega_u_xy_obs
        matrix[nb_crop + nb_input, nb_crop + nb_input] omega_u_xy_t = omega_u_xy / wdata[t];
        matrix[N_p, N_p] omega_u_xy_obs = quad_form(omega_u_xy_t, select_xy_obs') ; //select_xy_obs * omega_u_xy_t * select_xy_obs';

        // Extract zdelta_yxit for current time step
        matrix[nb_crop, nb_delta_y] Zt_DY = rep_matrix(0, nb_crop, nb_delta_y);

        int start_k = 1;
        int end_k = dim_crop_zpos_xy[1];
        for(k in 1:nb_crop){
          Zt_DY[k, start_k:end_k] = z_xy[t][mat_crop_zpos_xy[k][1:dim_crop_zpos_xy[k]]];
          if(k < nb_crop){
            start_k = start_k + dim_crop_zpos_xy[k];
            end_k = end_k  + dim_crop_zpos_xy[k+1];
          }

        }

        vector[nb_crop] zdelta_y = Zt_DY * delta_y;
        matrix[nb_crop, nb_input] zdelta_x = Zt_DY * delta_x;
        //for(j in 1:nb_input){
        // zdelta_x[, j] = Zt_DY * delta_x[,j];
        //}
        matrix[nb_crop, nb_input + 1] zdelta_yxit = append_col(zdelta_y, zdelta_x);

        // Compute beta_yxit
        matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

        matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

        // Compute yxapit using approx.yxit.fun
        matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


        // Compute error terms u_y and u_x
        vector[nb_input] u_x;
        vector[nb_crop] u_y = to_vector(y[t]) - yxapit[, 1];
        for (l in 1:nb_input) {
          u_x[l] = x[t, l] - dot_product(s[t], yxapit[, l + 1]);
        }

        // Compute u_xy_obs
        vector[N_p] u_xy_obs = select_xy_obs * append_row(u_x, u_y);

        // Compute log likelihood using dmnorm function
        real log_likelihood_obs = multi_normal_lpdf(u_xy_obs | rep_vector(0, N_p), omega_u_xy_obs);

        // Introduce a penality
        real log_likelihood_pen = 0.0;
        if(sigma_constr > 0){
          vector[nb_crop * (nb_input + 1)] vec_yxapit = to_vector(yxapit);

          vector[nb_crop * (nb_input + 1)] pmin_vec_yxapit; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
          for(m in 1:(nb_crop * (nb_input + 1))){
            pmin_vec_yxapit[m] = fmin(0, vec_yxapit[m]);
          }
          log_likelihood_pen = multi_normal_lpdf(pmin_vec_yxapit | rep_vector(0, nb_crop * (nb_input + 1)), diag_matrix(rep_vector(sigma_constr * sigma_constr, nb_crop * (nb_input + 1))));
        }else{
          log_likelihood_pen = 0;
        }

      // log likelihood
        log_likelihood += log_likelihood_obs + log_likelihood_pen;
      }
    }

    return log_likelihood;
  }


  // model allocation multivariate
  real multmodallx(int nb_crop,
              int nb_input,
              int nb_id,
              int nb_z_xy_max,
              int nb_delta_x,
              int nb_rp_x,
              matrix mat_s,
              matrix mat_x,
              vector mat_wdata,
              matrix mat_z_x,
              array[] int dim_it,
              array[,] int  mat_crop_zpos_x,
              array[] int  dim_crop_zpos_x,
              matrix omega_u_x,
              real sigma_constr,
              int distrib_method_beta_x,
              matrix delta_x,
              matrix mat_beta_i) {


    real log_likelihood = 0;

    int start_ind = 1;
    int end_ind = dim_it[1];
    for(i in 1:nb_id){
      int nb_T = dim_it[i];
      matrix[nb_T, nb_crop] s = mat_s[start_ind:end_ind];
      matrix[nb_T, nb_input] x = mat_x[start_ind:end_ind];
      vector[nb_T] wdata = mat_wdata[start_ind:end_ind];
      matrix[nb_T, nb_z_xy_max] z_x = mat_z_x[start_ind:end_ind];


      if(i < nb_id){
        start_ind = start_ind + dim_it[i];
        end_ind = end_ind + dim_it[i + 1];
      }


      //
      vector[nb_rp_x] beta_i = mat_beta_i[i]';
      matrix[nb_crop, nb_input] beta_xi_v;

      // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
      beta_xi_v = mod_rp_fun_beta_x(beta_i, nb_crop, nb_input, distrib_method_beta_x);

      for (t in 1:nb_T) {

        // Compute omega_u_xy_obs
        matrix[nb_input, nb_input] omega_u_x_t = omega_u_x / wdata[t];

        // Extract zdelta_yxit for current time step
        matrix[nb_crop, nb_delta_x] Zt_DX = rep_matrix(0, nb_crop, nb_delta_x);

        int start_k = 1;
        int end_k =  dim_crop_zpos_x[1];
        for(k in 1:nb_crop){
          Zt_DX[k, start_k:end_k] = z_x[t][ mat_crop_zpos_x[k][1: dim_crop_zpos_x[k]]];
          if(k < nb_crop){
            start_k = start_k +  dim_crop_zpos_x[k];
            end_k = end_k  +  dim_crop_zpos_x[k+1];
          }

        }

        matrix[nb_crop, nb_input] zdelta_x = Zt_DX * delta_x;

        // Compute beta_yxit
        matrix[nb_crop, nb_input] beta_xit = beta_xi_v + zdelta_x;


        // Compute yxapit using approx.yxit.fun
        matrix[nb_crop, nb_input] multxapit = beta_xit;


        // Compute error terms u_y and u_x
        vector[nb_input] u_x;
        for (l in 1:nb_input) {
          u_x[l] = x[t, l] - dot_product(s[t], multxapit[, l]);
        }

        // Compute log likelihood using dmnorm function
        real log_likelihood_obs = multi_normal_lpdf(u_x | rep_vector(0, nb_input), omega_u_x_t);

        // Introduce a penality
        real log_likelihood_pen = 0.0;
        if(sigma_constr > 0){
          vector[nb_crop * nb_input] vec_xapit = to_vector(multxapit);

          vector[nb_crop * nb_input] pmin_vec_xapit; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
          for(m in 1:(nb_crop * nb_input)){
            pmin_vec_xapit[m] = fmin(0, vec_xapit[m]);
          }
          log_likelihood_pen = multi_normal_lpdf(pmin_vec_xapit | rep_vector(0, nb_crop * nb_input), diag_matrix(rep_vector(square(sigma_constr), nb_crop * nb_input )));
        }else{
          log_likelihood_pen = 0;
        }

      // log likelihood
        log_likelihood += log_likelihood_obs + log_likelihood_pen;
      }
    }

    return log_likelihood;
  }

  // model allocation multivariate
  real imultmodallx(int nb_crop,
              int nb_input,
              int nb_T,
              int nb_delta,
              matrix s,
              matrix x,
              vector wdata,
              matrix z_x,
              array[,] int  mat_crop_zpos_x,
              array[] int  dim_crop_zpos_x,
              matrix omega_u_x,
              real sigma_constr,
              int distrib_method_beta_x,
              matrix delta,
              vector beta_i) {


    real log_likelihood = 0;
    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    matrix[nb_crop, nb_input] beta_xi_v = mod_rp_fun_beta_x(beta_i, nb_crop, nb_input, distrib_method_beta_x);
    for (t in 1:nb_T) {
      // Compute omega_u_xy_obs
      matrix[nb_input, nb_input] omega_u_x_t = omega_u_x / wdata[t];

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_delta] Zt_DX = rep_matrix(0, nb_crop, nb_delta);

      int start_k = 1;
      int end_k =  dim_crop_zpos_x[1];
      for(k in 1:nb_crop){
        Zt_DX[k, start_k:end_k] = z_x[t][mat_crop_zpos_x[k][1: dim_crop_zpos_x[k]]];
        if(k < nb_crop){
          start_k = start_k +  dim_crop_zpos_x[k];
          end_k = end_k  +  dim_crop_zpos_x[k+1];
        }

      }

      matrix[nb_crop, nb_input] zdelta = Zt_DX * delta;

      // Compute beta_yxit
      matrix[nb_crop, nb_input] beta_xit = beta_xi_v + zdelta;


      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input] multxapit = beta_xit;


      // Compute error terms u_y and u_x
      vector[nb_input] u_x;
      for (l in 1:nb_input) {
        u_x[l] = x[t, l] - dot_product(s[t], multxapit[, l]);
      }

      // Compute log likelihood using dmnorm function
      real log_likelihood_obs = multi_normal_lpdf(u_x | rep_vector(0, nb_input), omega_u_x_t);

      // Introduce a penality
      real log_likelihood_pen = 0.0;
      if(sigma_constr > 0){
        vector[nb_crop * nb_input] vec_xapit = to_vector(multxapit);

        vector[nb_crop * nb_input] pmin_vec_xapit; // = pmax(rep_vector(0.0, nb_crop), beta_byx);
        for(m in 1:(nb_crop * nb_input)){
          pmin_vec_xapit[m] = fmin(0, vec_xapit[m]);
        }
        log_likelihood_pen = multi_normal_lpdf(pmin_vec_xapit | rep_vector(0, nb_crop * nb_input), diag_matrix(rep_vector(square(sigma_constr), nb_crop * nb_input )));
      }else{
        log_likelihood_pen = 0;
      }

    // log likelihood
      log_likelihood += log_likelihood_obs + log_likelihood_pen;
    }

    return log_likelihood;
  }


  // sp acreages, s acreage share, sub: subsidies
  real dens_modyxs(matrix s,
              matrix x,
              matrix y,
              matrix sp,
              matrix sub,
              matrix iw_p,
              matrix piw,
              vector wdata,
              matrix zdelta_yx,
              matrix zdelta_s,
              matrix omega_u_yx,
              vector omega_u_s,
              int nb_crop,
              int nb_input,
              real lo_spher_x,
              real up_spher_x,
              real lo_spher_s,
              real up_spher_s,
              int distrib_method_beta_yx,
              int distrib_method_alpha_x,
              int distrib_method_alpha_s,
              vector beta_i) {

    int nb_T = rows(y);
    real log_likelihood = 0;
    vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);

    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);
    int idx_s  = idx_ax + nb_crop - 1;
    int idx_avs = idx_s + nb_crop - 1;
    int idx_aBs = idx_avs +  nb_crop * (nb_crop - 1)  / 2;

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];
    vector[nb_crop - 1] beta_si = beta_i[(idx_ax + 1):idx_s];
    vector[nb_crop - 1] beta_avsi = beta_i[(idx_s + 1):idx_avs];
    vector[nb_crop * (nb_crop - 1)  / 2] beta_aBsi = beta_i[(idx_avs + 1):idx_aBs];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;
    vector[nb_crop - 1] beta_si_v;
    vector[nb_crop - 1] beta_avsi_v;
    vector[nb_crop * (nb_crop - 1)  / 2] beta_aBsi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    beta_si_v   = beta_si;
    beta_avsi_v = beta_avsi;
    beta_aBsi_v  = to_vector(mod_rp_fun_alpha_x(beta_aBsi, 1, nb_crop - 1, lo_spher_s, up_spher_s, distrib_method_alpha_s));

    for (t in 1:nb_T) {
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
       regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
       start_l = start_l + nb_crop;
      }

      // Call mat_select_reg_obs function
      int N_p = sum(regimeyx);
      matrix[N_p, nb_crop *(nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      // Compute omega_u_yx_obs
      matrix[nb_crop *(nb_input + 1), nb_crop *(nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
      matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


      // Compute error terms u_y and u_x
      matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

      // Compute u_yx_obs
      vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

      // Compute log likelihood using dmnorm function
      real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

      // acreage model
      /* matrix[nb_crop, nb_crop] mat_H = vector_to_symmat(beta_sxi_v, nb_crop);
      vector[nb_crop - 1] vect_v = rep_vector(0, nb_crop - 1);
      matrix[nb_crop - 1, nb_crop - 1] mat_B = rep_matrix(0, nb_crop - 1, nb_crop - 1);

     for(k in 1:(nb_crop - 1)) {
        vect_v[k] = mat_H[k + 1, 1];
        for(j in 1:(nb_crop - 1)){
          mat_B[k, j] = mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1]);
        }
      }*/

      vector[nb_crop - 1] vect_v = beta_avsi_v;
      matrix[nb_crop - 1, nb_crop - 1] mat_B = vector_to_symmat(beta_aBsi_v, nb_crop-1);

      vector[nb_crop - 1] zdelta_sit = zdelta_s[t]';
      vector[nb_crop - 1] beta_sit = beta_si_v + zdelta_sit;

      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;

      vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));

      // regime in s
      array[nb_crop - 1] int regime_s ;
      regime_s = regime_name(s[t, 2:nb_crop]);


      // Call mat_select_reg_obs function
      int N_p_obs_s = sum(regime_s);
      int N_p_mis_s = nb_crop - 1  - N_p_obs_s;

      real log_likelihood_s = 0;
      if(N_p_obs_s == nb_crop - 1){

        vector[N_p_obs_s] u_s_obs = (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B * sp[t, 2:nb_crop]';
        vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;

        real log_likelihood_s_obs = 0;
        for(i in 1:N_p_obs_s){
          log_likelihood_s_obs += normal_lpdf(u_s_obs[i] | 0, sqrt(omega_u_s_t[i]) );
        }

        log_likelihood_s = log_determinant(mat_B) + log_likelihood_s_obs;

      }else if(N_p_obs_s == 0){

        vector[N_p_mis_s] g_s_mis = (marge_yx_s  - beta_sit - vect_v * sptot[t]) ;

        vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;

        real log_likelihood_s_prob_mis = 0;
        for(i in 1:N_p_mis_s){
          log_likelihood_s_prob_mis += normal_lcdf(- g_s_mis[i] | 0, sqrt(omega_u_s_t[i]) );
        }

        log_likelihood_s = log_likelihood_s_prob_mis;

      }else{

        matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
        matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

        matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
        matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


        vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
        vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';

        vector[nb_crop - 1] omega_u_s_t = omega_u_s / wdata[t] ;
        vector[N_p_obs_s] omega_u_s_obs = select_s_obs * omega_u_s_t;
        vector[N_p_mis_s] omega_u_s_mis = select_s_mis * omega_u_s_t;


        real log_likelihood_s_obs = 0;
        real log_likelihood_s_prob_mis = 0;
        for(i in 1:N_p_obs_s){
          log_likelihood_s_obs += normal_lpdf(u_s_obs[i] | 0, sqrt(omega_u_s_obs[i]) );
        }
        for(i in 1:N_p_mis_s){
          log_likelihood_s_prob_mis += normal_lcdf(- g_s_mis[i] | 0, sqrt(omega_u_s_mis[i]) );
        }

        log_likelihood_s = log_determinant(mat_B_obs) + log_likelihood_s_obs + log_likelihood_s_prob_mis;
      }

      // log likelihood
      log_likelihood += log_likelihood_xy_obs + log_likelihood_s;
    }

    return log_likelihood;
  }

  //
   // sp acreages, s acreage share, sub: subsidies
  real dens_modyx(matrix s,
              matrix x,
              matrix y,
              matrix iw_p,
              matrix piw,
              vector wdata,
              matrix zdelta_yx,
              matrix omega_u_yx,
              int nb_crop,
              int nb_input,
              real lo_spher_x,
              real up_spher_x,
              int distrib_method_beta_yx,
              int distrib_method_alpha_x,
              vector beta_i) {

    int nb_T = rows(y);
    real log_likelihood = 0;

    int idx_yx = nb_crop * (nb_input + 1);
    int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1)  / 2);

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1)  / 2] beta_axi = beta_i[(idx_yx + 1):idx_ax];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1)  / 2] beta_axi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);


    for (t in 1:nb_T) {
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
       regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
       start_l = start_l + nb_crop;
      }

      // Call mat_select_reg_obs function
      int N_p = sum(regimeyx);
      matrix[N_p, nb_crop *(nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);

      // Compute omega_u_yx_obs
      matrix[nb_crop *(nb_input + 1), nb_crop *(nb_input + 1)] omega_u_yx_t = omega_u_yx / wdata[t];
      matrix[N_p, N_p] omega_u_yx_obs = quad_form(omega_u_yx_t, select_yx_obs') ;//select_yx_obs * omega_u_yx_t * select_yx_obs';

      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);


      // Compute error terms u_y and u_x
      matrix[nb_crop, nb_input + 1] u_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

      // Compute u_yx_obs
      vector[N_p] u_yx_obs = select_yx_obs * to_vector(u_yx);

      // Compute log likelihood using dmnorm function
      real log_likelihood_xy_obs = multi_normal_lpdf(u_yx_obs | rep_vector(0, N_p), omega_u_yx_obs);

      // log likelihood
      log_likelihood += log_likelihood_xy_obs ;
    }

    return log_likelihood;
  }

}
