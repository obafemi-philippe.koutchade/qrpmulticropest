#include include/myfunctions.stan

// The input data is a vector 'y' of length 'N'.
data {
  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_rp] m_beta_b;
  matrix[nb_rp, nb_rp] omega_b;
  vector[nb_T] wdata;
  matrix[nb_crop * (nb_input + 1) , nb_crop * (nb_input + 1)] omega_u_yx;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  real lo_spher_x;
  real up_spher_x;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
}

// The parameters accepted by the model. Our model
// accepts two parameters 'mu' and 'sigma'.
parameters {
  vector[nb_rp] beta_i;
}

model {
  beta_i ~ multi_normal(m_beta_b, omega_b);
  target +=  dens_modyx(s,
                       x,
                       y,
                       iw_p,
                       piw,
                       wdata,
                       zdelta_yx,
                       omega_u_yx,
                       nb_crop,
                       nb_input,
                       lo_spher_x,
                       up_spher_x,
                       distrib_method_beta_yx,
                       distrib_method_alpha_x,
                       beta_i);

}
