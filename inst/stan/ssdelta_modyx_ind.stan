
#include /inst/stan/include/myfunctions.stan

data {

  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  int<lower=0> nb_sim;
  matrix[nb_sim, nb_rp] beta_i;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_T] wdata;
  matrix[(nb_input + 1) * nb_crop, (nb_input + 1) * nb_crop] omega_u_yx;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  real lo_spher_x;
  real up_spher_x;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  int score;
  matrix[nb_rp, nb_rp] omega_b;
  int<lower=0> nb_zbd;
  matrix[nb_rp * nb_rp, nb_rp * (nb_rp +1)/2] dup_omega_b;
  matrix[nb_rp, nb_zbd] mat_bd;
  vector[nb_rp] m_beta;

  int<lower=0> nb_zyx;
  matrix[nb_crop * (nb_input + 1) * nb_crop * (nb_input + 1), nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2] dup_omega_u_yx;
  matrix[nb_T * nb_crop * (nb_input + 1), nb_zyx] bigmat_yx;
  vector[nb_zyx] delta_yx;

}

model {

}

generated quantities{

  int idx_yx = nb_crop * (nb_input + 1);
  int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1) / 2);

  matrix[nb_T, nb_crop * (nb_input +1 )] ss_rp_yx = rep_matrix(0, nb_T, nb_crop * (nb_input +1 ));
  matrix[nb_crop * (nb_input +1 ), nb_crop * (nb_input +1 )] ss_rp_yx2= rep_matrix(0, nb_crop * (nb_input +1 ), nb_crop * (nb_input +1 ));

  vector[nb_zyx] score_delta_yx;
  vector[nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2 ] score_omega_u_yx;
  score_omega_u_yx = rep_vector(0,nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2);
  score_delta_yx = rep_vector(0, nb_zyx);

  vector[nb_zbd] score_beta_bd;
  vector[nb_rp * (nb_rp + 1)/2] score_omega_b;
  score_omega_b = rep_vector(0, nb_rp * (nb_rp + 1)/2);
  score_beta_bd = rep_vector(0, nb_zbd);

  //
  array[nb_T] matrix[nb_crop * (nb_input + 1), nb_zyx] bigmat_yx_t;
  int start_t = 1;
  for(l in 1:nb_T){
    bigmat_yx_t[l] = bigmat_yx[start_t:(start_t + nb_crop * (nb_input + 1) - 1), ];
    start_t = start_t + nb_crop * (nb_input + 1);
  }

  for(r in 1:nb_sim){

    vector[nb_rp] beta_i_r = beta_i[r]';

    /********** Computation of score beta et omega *****************************/
    if(score == 1){
      score_beta_bd += mat_bd' * inverse(omega_b) * (beta_i_r - m_beta) / nb_sim;

      matrix[nb_rp , nb_rp] val_tp_b;
      val_tp_b = inverse(omega_b) - inverse(omega_b) *
                  (beta_i_r * beta_i_r' - m_beta * beta_i_r' - beta_i_r * m_beta' +
                  m_beta * m_beta') * inverse(omega_b);
      score_omega_b +=  - 0.5 * dup_omega_b' * to_vector(val_tp_b) / nb_sim;
    }
    /***************************************************************************/

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i_r[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1) / 2] beta_axi = beta_i_r[(idx_yx + 1):idx_ax];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1) / 2] beta_axi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v  = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);

    matrix[nb_T, nb_crop * (nb_input + 1)] ss_yx_obs_delta;
    for (t in 1:nb_T) {
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
       regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
       start_l = start_l + nb_crop;
      }

      // Call mat_select_reg_obs function
      int N_p_obs = sum(regimeyx);
      int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
      matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);
      matrix[N_p_mis, nb_crop * (nb_input + 1)] select_yx_mis = mat_select_reg_mis(regimeyx);

      // Compute omega_u_yx_obs
      matrix[N_p_obs, N_p_obs] omega_u_yx_obs = quad_form(omega_u_yx, select_yx_obs'); //select_yx_obs * omega_u_yx * select_yx_obs';

      matrix[N_p_obs, N_p_mis] omega_u_yx_obs_mis = select_yx_obs * omega_u_yx * select_yx_mis';
      matrix[N_p_mis, N_p_obs] delta_xy_cond_mis_obs = mdivide_right_spd(omega_u_yx_obs_mis', omega_u_yx_obs);
      matrix[nb_crop * (nb_input + 1), N_p_obs] mat_delta_xy = select_yx_obs' + select_yx_mis' * delta_xy_cond_mis_obs;


      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxi_v, beta_axi_v, nb_crop, nb_input);


      // Compute terms ss_y and ss_x
      matrix[nb_crop, nb_input + 1] ss_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

      // Compute ss_yx_obs_delta
      vector[N_p_obs] ss_yx_obs = select_yx_obs * to_vector(ss_yx);
      vector[nb_crop * (nb_input + 1)] ss_yx_obs_delta_t = mat_delta_xy * ss_yx_obs;
      ss_yx_obs_delta[t]  = ss_yx_obs_delta_t';


      // Computation of score
      if(score == 1){
        matrix[nb_crop * (nb_input + 1), nb_zyx ] mat_yx_tp = bigmat_yx_t[t];

        matrix[nb_crop * (nb_input + 1), nb_zyx] mat_yx = mat_delta_xy * select_yx_obs * mat_yx_tp;
        score_delta_yx += mat_yx' * inverse(omega_u_yx) * (ss_yx_obs_delta_t - mat_yx * delta_yx) / nb_sim;

        matrix[nb_crop * (nb_input + 1), nb_crop * (nb_input + 1)] val_tp;
        val_tp = inverse(omega_u_yx) - inverse(omega_u_yx) *
                    (ss_yx_obs_delta_t * ss_yx_obs_delta_t' -
                    (mat_yx * delta_yx) * ss_yx_obs_delta_t' -
                    ss_yx_obs_delta_t * (mat_yx * delta_yx)' +
                    (mat_yx * delta_yx) * (mat_yx * delta_yx)') * inverse(omega_u_yx);
        score_omega_u_yx +=  - 0.5 * dup_omega_u_yx' * to_vector(val_tp) / nb_sim;

      }
    }

    ss_rp_yx += ss_yx_obs_delta / nb_sim ;
    ss_rp_yx2 += ss_yx_obs_delta' * diag_matrix(wdata) * ss_yx_obs_delta / nb_sim ;

  }

}

