
#include /inst/stan/include/myfunctions.stan

data {

  int<lower=0> nb_crop;
  int<lower=0> nb_input;
  int<lower=0> nb_T;
  int<lower=0> nb_rp;
  int<lower=0> nb_sim;
  matrix[nb_sim, nb_rp] beta_i;
  matrix[nb_T, nb_crop] s;
  matrix[nb_T, nb_input * nb_crop] x;
  matrix[nb_T, nb_crop] y;
  matrix[nb_T, nb_crop] sp;
  matrix[nb_T, nb_crop] sub;
  matrix[nb_T, nb_crop * nb_input] iw_p;
  matrix[nb_T, nb_crop * (nb_input + 1)] piw;
  vector[nb_T] wdata;
  matrix[(nb_input + 1) * nb_crop, (nb_input + 1) * nb_crop] omega_u_yx;
  matrix[nb_T, nb_crop * (nb_input + 1)] zdelta_yx;
  matrix[nb_T, nb_crop - 1] zdelta_s;
  real lo_spher_x;
  real up_spher_x;
  real lo_spher_s;
  real up_spher_s;
  int distrib_method_beta_yx;
  int distrib_method_alpha_x;
  int distrib_method_alpha_s;
  int score;
  matrix[nb_rp, nb_rp] omega_b;
  int<lower=0> nb_zbd;
  matrix[nb_rp * nb_rp, nb_rp * (nb_rp +1)/2] dup_omega_b;
  matrix[nb_rp, nb_zbd] mat_bd;
  vector[nb_rp] m_beta;

  int<lower=0> nb_zyx;
  matrix[nb_crop * (nb_input + 1) * nb_crop * (nb_input + 1), nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2] dup_omega_u_yx;
  matrix[nb_T * nb_crop * (nb_input + 1), nb_zyx] bigmat_yx;
  vector[nb_zyx] delta_yx;

}

model {

}

generated quantities{

  vector[nb_T] sptot = sp * rep_vector(1.0, nb_crop);

  int idx_yx = nb_crop * (nb_input + 1);
  int idx_ax = idx_yx + (nb_crop * nb_input * (nb_input + 1) / 2);
  int idx_s  = idx_ax + nb_crop - 1;
  int idx_avs  = idx_s + nb_crop - 1;
  int idx_aBs = idx_avs +  nb_crop * (nb_crop - 1) / 2;

  matrix[nb_T, nb_crop * (nb_input +1 )] ss_rp_yx = rep_matrix(0, nb_T, nb_crop * (nb_input +1 ));
  matrix[nb_crop * (nb_input +1 ), nb_crop * (nb_input +1 )] ss_rp_yx2= rep_matrix(0, nb_crop * (nb_input +1 ), nb_crop * (nb_input +1 ));
  matrix[nb_T, nb_crop - 1] ss_rp_s = rep_matrix(0, nb_T, nb_crop - 1 );

  vector[nb_zyx] score_delta_yx;
  vector[nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2 ] score_omega_u_yx;
  score_omega_u_yx = rep_vector(0,nb_crop * (nb_input + 1) * (nb_crop * (nb_input + 1) +1)/2);
  score_delta_yx = rep_vector(0, nb_zyx);

  vector[nb_zbd] score_beta_bd;
  vector[nb_rp * (nb_rp + 1)/2] score_omega_b;
  score_omega_b = rep_vector(0, nb_rp * (nb_rp + 1)/2);
  score_beta_bd = rep_vector(0, nb_zbd);

  //
  array[nb_T] matrix[nb_crop * (nb_input + 1), nb_zyx] bigmat_yx_t;
  int start_t = 1;
  for(l in 1:nb_T){
    bigmat_yx_t[l] = bigmat_yx[start_t:(start_t + nb_crop * (nb_input + 1) - 1), ];
    start_t = start_t + nb_crop * (nb_input + 1);
  }

  for(r in 1:nb_sim){

    vector[nb_rp] beta_i_r = beta_i[r]';

    /********** Computation of score beta et omega *****************************/
    if(score == 1){
      score_beta_bd += mat_bd' * inverse(omega_b) * (beta_i_r - m_beta) / nb_sim;

      matrix[nb_rp , nb_rp] val_tp_b;
      val_tp_b = inverse(omega_b) - inverse(omega_b) *
                  (beta_i_r * beta_i_r' - m_beta * beta_i_r' - beta_i_r * m_beta' +
                  m_beta * m_beta') * inverse(omega_b);
      score_omega_b +=  - 0.5 * dup_omega_b' * to_vector(val_tp_b) / nb_sim;
    }
    /***************************************************************************/

    vector[nb_crop * (nb_input + 1)] beta_yxi = beta_i_r[1:idx_yx];
    vector[nb_crop * nb_input * (nb_input + 1) / 2] beta_axi = beta_i_r[(idx_yx + 1):idx_ax];
    vector[nb_crop - 1] beta_si = beta_i_r[(idx_ax + 1):idx_s];
    vector[nb_crop - 1] beta_avsi = beta_i_r[(idx_s + 1):idx_avs];
    vector[nb_crop * (nb_crop - 1) / 2] beta_aBsi = beta_i_r[(idx_avs + 1):idx_aBs];


    matrix[nb_crop, (nb_input + 1)] beta_yxi_v;
    matrix[nb_crop, nb_input * (nb_input + 1) / 2] beta_axi_v;
    vector[nb_crop - 1] beta_si_v;
    vector[nb_crop - 1] beta_avsi_v;
    vector[nb_crop * (nb_crop - 1) / 2] beta_aBsi_v;

    // Call the corresponding functions to compute beta_yxi_v and beta_axi_v
    beta_yxi_v  = mod_rp_fun_beta_xy(beta_yxi, nb_crop, nb_input, distrib_method_beta_yx);
    beta_axi_v  = mod_rp_fun_alpha_x(beta_axi, nb_crop, nb_input, lo_spher_x, up_spher_x, distrib_method_alpha_x);
    beta_si_v   = beta_si;
    beta_avsi_v   = beta_avsi;
    beta_aBsi_v  = to_vector(mod_rp_fun_alpha_x(beta_aBsi, 1, nb_crop - 1, lo_spher_s, up_spher_s, distrib_method_alpha_s));

    matrix[nb_T, nb_crop * (nb_input + 1)] ss_yx_obs_delta;
    matrix[nb_T, nb_crop -1] ss_s_obs_mis;
    for (t in 1:nb_T) {
      // Extract regime name and create regimeyx
      array[nb_crop] int regimey = regime_name(s[t]);
      array[nb_crop + nb_crop * nb_input] int regimeyx;
      int start_l = 1;
      for(l in 1:(nb_input + 1)){
       regimeyx[start_l:(start_l + nb_crop - 1)] = regimey;
       start_l = start_l + nb_crop;
      }

      // Call mat_select_reg_obs function
      int N_p_obs = sum(regimeyx);
      int N_p_mis = nb_crop * (nb_input + 1) - N_p_obs;
      matrix[N_p_obs, nb_crop * (nb_input + 1)] select_yx_obs = mat_select_reg_obs(regimeyx);
      matrix[N_p_mis, nb_crop * (nb_input + 1)] select_yx_mis = mat_select_reg_mis(regimeyx);

      // Compute omega_u_yx_obs
      matrix[N_p_obs, N_p_obs] omega_u_yx_obs = quad_form(omega_u_yx, select_yx_obs'); //select_yx_obs * omega_u_yx * select_yx_obs';

      matrix[N_p_obs, N_p_mis] omega_u_yx_obs_mis = select_yx_obs * omega_u_yx * select_yx_mis';
      matrix[N_p_mis, N_p_obs] delta_xy_cond_mis_obs = mdivide_right_spd(omega_u_yx_obs_mis', omega_u_yx_obs);
      matrix[nb_crop * (nb_input + 1), N_p_obs] mat_delta_xy = select_yx_obs' + select_yx_mis' * delta_xy_cond_mis_obs;


      matrix[nb_crop, nb_input] w_pit = to_matrix(iw_p[t], nb_crop, nb_input);

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit = approx_yxit_fun(w_pit, beta_yxi_v, beta_axi_v, nb_crop, nb_input);


      // Compute terms ss_y and ss_x
      matrix[nb_crop, nb_input + 1] ss_yx = append_col(to_vector(y[t]), to_matrix(x[t], nb_crop, nb_input)) - yxapit;

      // Compute ss_yx_obs_delta
      vector[N_p_obs] ss_yx_obs = select_yx_obs * to_vector(ss_yx);
      vector[nb_crop * (nb_input + 1)] ss_yx_obs_delta_t = mat_delta_xy * ss_yx_obs;
      ss_yx_obs_delta[t]  = ss_yx_obs_delta_t';


      // Computation of score
      if(score == 1){
        matrix[nb_crop * (nb_input + 1), nb_zyx ] mat_yx_tp = bigmat_yx_t[t];

        matrix[nb_crop * (nb_input + 1), nb_zyx] mat_yx = mat_delta_xy * select_yx_obs * mat_yx_tp;
        score_delta_yx += mat_yx' * inverse(omega_u_yx) * (ss_yx_obs_delta_t - mat_yx * delta_yx) / nb_sim;

        matrix[nb_crop * (nb_input + 1), nb_crop * (nb_input + 1)] val_tp;
        val_tp = inverse(omega_u_yx) - inverse(omega_u_yx) *
                  (ss_yx_obs_delta_t * ss_yx_obs_delta_t' -
                  (mat_yx * delta_yx) * ss_yx_obs_delta_t' -
                  ss_yx_obs_delta_t * (mat_yx * delta_yx)' +
                  (mat_yx * delta_yx) * (mat_yx * delta_yx)') * inverse(omega_u_yx);
        score_omega_u_yx +=  - 0.5 * dup_omega_u_yx' * to_vector(val_tp) / nb_sim;
      }

      /////////////////////////////////////////////////////////////////////////

      // Marge
      // Extract zdelta_yxit for current time step
      matrix[nb_crop, nb_input + 1] zdelta_yxit = to_matrix(zdelta_yx[t], nb_crop, (nb_input + 1));

      // Compute beta_yxit
      matrix[nb_crop, nb_input + 1] beta_yxit = beta_yxi_v + zdelta_yxit;

      // Compute yxapit using approx.yxit.fun
      matrix[nb_crop, nb_input + 1] yxapit_m = approx_yxit_fun(w_pit, beta_yxit, beta_axi_v, nb_crop, nb_input);

      matrix[nb_crop, nb_input + 1] piwit = to_matrix(piw[t], nb_crop, nb_input + 1);
      matrix[nb_crop, nb_input + 1] marge_yx_tp = yxapit_m .* piwit;
      vector[nb_crop ] marge_yx = sub[t]' + marge_yx_tp[,1] - marge_yx_tp[, 2:(nb_input + 1)] * rep_vector(1.0, nb_input) ;

      vector[nb_crop - 1] marge_yx_s = marge_yx[2:nb_crop] - rep_vector(marge_yx[1], (nb_crop - 1));


      // acreage model
      vector[nb_crop - 1] beta_sit = beta_si_v ;

      // Matrix B and vector v
      /*matrix[nb_crop, nb_crop] mat_H = vector_to_symmat(beta_sxi_v, nb_crop);
      vector[nb_crop - 1] vect_v = rep_vector(0, nb_crop - 1);
      matrix[nb_crop - 1, nb_crop - 1] mat_B = rep_matrix(0, nb_crop - 1, nb_crop - 1);

      for(k in 1:(nb_crop - 1)) {
        vect_v[k] = mat_H[k + 1, 1];
        for(j in 1:(nb_crop - 1)){
          mat_B[k, j] = mat_H[k + 1, j + 1] - mat_H[k + 1, 1] - (mat_H[1, j + 1] - mat_H[1, 1]);
        }
      }*/

      vector[nb_crop - 1] vect_v = beta_avsi_v;
      matrix[nb_crop - 1, nb_crop - 1] mat_B = vector_to_symmat(beta_aBsi_v, nb_crop-1);

      // regime and
      int regime_s[nb_crop - 1] ;
      regime_s = regime_name(s[t,2:nb_crop]);

      // Call mat_select_reg_obs function
      int N_p_obs_s = sum(regime_s);
      int N_p_mis_s = nb_crop - 1  - N_p_obs_s;
      matrix[N_p_obs_s, nb_crop - 1] select_s_obs = mat_select_reg_obs(regime_s);
      matrix[N_p_mis_s, nb_crop - 1] select_s_mis = mat_select_reg_mis(regime_s);

      matrix[N_p_obs_s, N_p_obs_s] mat_B_obs = select_s_obs * mat_B * select_s_obs';
      matrix[N_p_mis_s, N_p_obs_s] mat_B_mis_obs = select_s_mis * mat_B * select_s_obs';


      vector[N_p_obs_s] u_s_obs = select_s_obs * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_obs * select_s_obs * sp[t, 2:nb_crop]';
      vector[N_p_mis_s] g_s_mis = select_s_mis * (marge_yx_s  - beta_sit - vect_v * sptot[t]) - mat_B_mis_obs * select_s_obs * sp[t, 2:nb_crop]';

      ss_s_obs_mis[t] = (select_s_obs' * u_s_obs + select_s_mis' * g_s_mis)';
    }

    ss_rp_yx += ss_yx_obs_delta / nb_sim ;
    ss_rp_yx2 += ss_yx_obs_delta' * diag_matrix(wdata) * ss_yx_obs_delta / nb_sim ;

    ss_rp_s += ss_s_obs_mis / nb_sim ;

  }

}

